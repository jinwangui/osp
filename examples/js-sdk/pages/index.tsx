import React from "react";
import { App } from "../widget/app/App";
import { ClientOptions, connect } from "osp-client-js";
const _env = process.env.NEXT_PUBLIC_API_ENV;

const env = {
  api_env: _env,
};

const options: ClientOptions = {
  urlOverride: {
    api:
      typeof location !== "undefined"
        ? location?.origin + `/${env.api_env}_api/v2`
        : "",
    imageUrl: "https://test.trexprotocol.com",
  },
  timeout: 10000,
  browser: true,
  version: "",
  env: env.api_env, //'dev' or 'beta'
  error: (error) => {
    console.log(error);
  },
};

export default function Example() {
  const [client, setClient] = React.useState();
  React.useEffect(() => {
    const client = connect("key", "", "app_id", options);
    console.log("client", client.address, "sadasdasd");
    setClient(client);
  }, []);
  if (!client) return <div>loading...</div>;
  return <App client={client} />;
}

/**
 * TODO LIST（后端暂未完成）:
 *
 *  Auth模块
 *    暂无
 *
 *  User模块
 *    1. 删除Profile
 *    2. 更新Profile
 *    3. user dispatcher
 *
 *  Activity模块
 *    1. 删除用户的Activity
 *
 *  Relation模块
 *    1. 取消关注
 *    2. 批量关注
 *
 *  Reaction模块
 *    1. 删除评论 & 回复
 *    2. 取消收藏
 *    3. 点赞（kind = LIKE），不上链的已调完，上链的后端暂未开发
 *    4. 取消点赞
 *    4. 分享（kind = SHARE）
 *    5. 投票（kind = UPVOTE）
 *    6. 取消投票（kind = DONWVOTE）
 *
 *  TypeData模块
 *    1. /v2/typedata/profile/dispatcher
 */
