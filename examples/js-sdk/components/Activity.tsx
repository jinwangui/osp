import React, { useState } from "react";
import {
  Client,
  ReactionKindEnum,
  ContentTypeEnum,
  OnChainTypeEnum,
} from "osp-client-js";

const communityId = "0x98968b";

export const Activity = ({ client }: { client: Client }) => {
  const profileId = client.profile.profileId;
  const [activityList, setActivityList] = useState([]);
  const [feedList, setFeedList] = useState([]);
  const [image, setImage] = useState("");
  const [imageResize, setImageResize] = useState("");
  const uploadImage = async () => {
    const fileUpload = document.getElementById("fileUpload");

    const file = fileUpload.files[0];
    const res = await client.ipfs.upload(file);
    console.log(res);
    setImage(
      `${res.replace("ipfs://", "https://test.trexprotocol.com/ipfs/")}`
    );
    setImageResize(
      `${res.replace(
        "ipfs://",
        "https://test.trexprotocol.com/cdn-cgi/image/width=80,quality=75/ipfs/"
      )}`
    );
  };
  const createActivity = async () => {
    const metadata: any = {
      content: "hihi" + Math.random(),
      image: [
        [
          "https://test.trexprotocol.com/ipfs/QmfWh3PKPbXd2hQz7iQbjqTFitTDExTNQ1rVP39QK9FkXM",
          "jpeg",
        ],
      ],
    };

    await client.activity.addActivity({
      communityId,
      metadata,
      isOnChain: false,
    });
  };

  const createActivityOnChainOnly = async (text) => {
    const metadata: any = {
      content: text,
    };

    const res = await client.activity.addActivity({
      communityId,
      metadata,
      isOnChain: true,
      onChainType: OnChainTypeEnum.ONLY_ON_CHAIN,
    });

    console.log("res", res);
  };

  // 导入测试用户 0x47e179ec197488593b187f80a00eb0da91f1b9d0b13f8733639f19c30a34926a
  const createActivityPostWithSig = async () => {
    const metadata: any = {
      content: "hihi" + Math.random(),
      image: [
        [
          "https://test.trexprotocol.com/ipfs/QmfWh3PKPbXd2hQz7iQbjqTFitTDExTNQ1rVP39QK9FkXM",
          "jpeg",
        ],
      ],
    };

    client.activity
      .addActivity({
        communityId,
        metadata,
        isOnChain: true,
        onChainType: OnChainTypeEnum.ON_CHAIN,
      })
      .then((res) => console.log(res));
  };

  const getAllStories = async () => {
    const res = await client.activity.stories(profileId, {
      limit: 20,
      next_token: "",
      profile_id: "",
      zones: [""],
    });
    setActivityList(res?.data?.rows || []);
  };

  const getHomeFeeds = async () => {
    const res = await client.activity.feeds("aggregated", "home", {
      limit: 20,
      next_token: "",
      communityId,
    });
    console.log("ppppp", res);
    setFeedList(res?.data?.rows || []);
  };

  const getTopFeeds = async () => {
    const res = await client.activity.feeds("aggregated", "top", {
      limit: 20,
      next_token: "",
      communityId,
    });
    console.log("ppppp", res);
    setFeedList(res?.data?.rows || []);
  };
  const getUserFeeds = async () => {
    const res = await client.activity.feeds("aggregated", "user_post", {
      limit: 20,
      next_token: "",
      profileId,
    });
    console.log("ppppp", res);
    setFeedList(res?.data?.rows || []);
  };

  const getFeedDetail = async (view_id) => {
    const res = await client.activity.getFeedDetail(view_id, {
      enrich: true,
      withRecentReactions: true,
      recentReactionsLimit: 20,
      withOwnReactions: true,
      withOwnChildren: true,
      withReactionCounts: true,
      reactionKindsFilter: [""],
      with_profile: true,
      with_community: true,
      with_relations: ["JOIN"],
    });
    console.log("ppppp", res);
  };

  const [commentId, setCommentId] = useState("");
  const [commentContent, setCommentContent] = useState("");
  const [replyContent, setReplyContent] = useState("");
  const handleReply = async (profile_id, view_id, content_id, kind) => {
    //upload to ipfs
    if (!commentId) return alert("please comment first!");

    const metadata: any = {
      content: "回复" + Math.random(),
    };

    const body = {
      view_id: commentId,
      profile_id,
      metadata,
      isOnChain: false,
    };

    const { error, data }: any = await client.reaction.add(kind, body);
    console.log("handleReply info", data, error);
    setReplyContent(metadata.content);
  };
  const handleReplyOnChain = async (
    profile_id,
    view_id,
    content_id,
    kind,
    reaction_kind
  ) => {
    //upload to ipfs
    if (!content_id) return alert("this is a off chain feed!");

    const metadata: any = {
      content: `${profile_id}回复${Math.random()}`,
    };

    const body = {
      view_id,
      content_id,
      profile_id,
      metadata,
      isOnChain: true,
      reaction_kind,
      onChainType: OnChainTypeEnum.ON_CHAIN,
    };

    const { error, data }: any = await client.reaction.add(kind, body);
    console.log("handleReply info", data, error);
  };

  const handleReplyOnChainOnly = async (
    profile_id,
    view_id,
    content_id,
    kind,
    reaction_kind
  ) => {
    //upload to ipfs
    if (!content_id) return alert("this is a off chain feed!");
    if (!replyContent) return alert("please off chain reply first!");

    const metadata: any = {
      content: `${profile_id}回复${Math.random()}`,
    };

    const body = {
      view_id,
      content_id,
      profile_id,
      metadata,
      isOnChain: true,
      reaction_kind,
      onChainType: OnChainTypeEnum.ONLY_ON_CHAIN,
    };

    const { error, data }: any = await client.reaction.add(kind, body);
    console.log("handleReply info", data, error);
    setReplyContent("");
  };

  const handleComment = async (profile_id, view_id, content_id, kind) => {
    //upload to ipfs
    const metadata: any = {
      content: "评论" + Math.random(),
    };

    const body = {
      view_id: view_id,
      profile_id,
      metadata,
      isOnChain: false,
    };

    const { error, data }: any = await client.reaction.add(kind, body);
    console.log("handleComment info", data, error);
    setCommentId(data.id);
    setCommentContent(metadata.content);
  };

  const handleCommentOnChainOnly = async (
    profile_id,
    view_id,
    content_id,
    contentKind,
    reaction_kind
  ) => {
    if (!content_id) return alert("this is a off chain feed!");
    if (!commentContent) return alert("please off chain reply first!");

    //upload to ipfs
    const profileId = client.profile.profileId;
    // content should be from offChain comment content
    const metadata: any = {
      content: commentContent,
    };

    setCommentContent("");

    const body = {
      view_id,
      profile_id,
      metadata,
      content_id,
      isOnChain: true,
      reaction_kind,
      onChainType: OnChainTypeEnum.ONLY_ON_CHAIN,
    };

    const { error, data }: any = await client.reaction.add(contentKind, body);
    console.log("handleComment info", data, error);
  };

  const handleCommentOnChain = async (
    profile_id,
    view_id,
    content_id,
    contentKind,
    reaction_kind
  ) => {
    //upload to ipfs
    const profileId = client.profile.profileId;
    const metadata: any = {
      content: `${profile_id}评论${Math.random()}`,
    };

    const body = {
      view_id,
      profile_id,
      metadata,
      content_id,
      isOnChain: true,
      reaction_kind,
      onChainType: OnChainTypeEnum.ON_CHAIN,
    };

    const { error, data }: any = await client.reaction.add(contentKind, body);
    console.log("handleComment info", data, error);
  };

  const [commentsList, setCommentsList] = useState([]);
  const [replyList, setReplyList] = useState([]);

  const handleGetCommentsList = async (view_id, kind) => {
    const lookupAttr = "REACTION";
    const lookupValue = view_id;
    const query = {
      kind,
      ranking: "TIME",
      limit: 20,
      next_token: "",
      with_activity: false,
      with_profile: true,
    };
    const { error, data }: any = await client.reaction.get(
      lookupAttr,
      lookupValue,
      kind,
      query
    );
    if (kind === ReactionKindEnum.COMMENT) {
      setCommentsList(data?.rows || []);
    } else {
      setReplyList(data?.rows || []);
    }
    console.log("handleGetFollowingList info", data, error);
  };
  const handleDoLike = async (
    profile_id,
    view_id,
    content_id,
    kind,
    isOnChain = false
  ) => {
    const body = {
      view_id,
      profile_id,
      isOnChain,
    };

    const { error, data }: any = await client.reaction.add(kind, body);
    console.log("handleGetFollowingList info", data, error);
  };

  const handleDoLikeOnChainOnly = async (
    profile_id,
    view_id,
    content_id,
    kind,
    isOnChain = true
  ) => {
    if (!content_id) return alert("this is a off chain feed!");
    const body = {
      content_id,
      view_id,
      profile_id,
      isOnChain,
      onChainType: OnChainTypeEnum.ONLY_ON_CHAIN,
    };

    const { error, data }: any = await client.reaction.add(kind, body, {
      like: {
        is_like: true,
      },
    });
    console.log("handleGetFollowingList info", data, error);
  };

  const handleDoLikeOnChain = async (
    profile_id,
    view_id,
    content_id,
    kind,
    isOnChain = true
  ) => {
    if (!content_id) return alert("this is a off chain feed!");
    const body = {
      content_id,
      view_id,
      profile_id,
      isOnChain,
      onChainType: OnChainTypeEnum.ON_CHAIN,
    };

    const { error, data }: any = await client.reaction.add(kind, body, {
      like: {
        is_like: true,
      },
    });
    console.log("handleGetFollowingList info", data, error);
  };

  const handleUnDoLike = async (view_id, content_id, kind, isOnChain) => {
    const lookupAttr = "REACTION";
    const lookupValue = view_id;
    const { error, data }: any = await client.reaction.deleteByKind(
      lookupAttr,
      lookupValue,
      kind
    );
    console.log("handleGetFollowingList info", data, error);
  };
  const handleUnDoLikeOnChain = async (
    profile_id,
    view_id,
    content_id,
    kind,
    isOnChain
  ) => {
    if (!content_id) return alert("this is a off chain feed!");
    const body = {
      content_id,
      view_id,
      profile_id,
      isOnChain,
    };

    const { error, data }: any = await client.reaction.add(kind, body, {
      like: {
        is_like: false,
      },
    });
    console.log("handleGetFollowingList info", data, error);
  };

  if (!profileId) return;
  return (
    <div>
      <h2>Activity模块</h2>
      <h3>上传图片</h3>
      <input
        id="fileUpload"
        type="file"
        name="myImage"
        onChange={uploadImage}
        accept="image/png, image/gif, image/jpeg,  image/avif,  image/webp"
      />
      {image ? (
        <>
          <span>原图</span>
          <img src={image} width="100" />
          <span>图片缩放</span>
          <img src={imageResize} />{" "}
        </>
      ) : null}
      <h3>创建用户的Activity---包含上链</h3>
      <button onClick={createActivityPostWithSig}>创建Acvitity-包含上链</button>
      <br />
      <h3>创建用户的Activity--不包含上链</h3>
      <button onClick={createActivity}>创建Acvitity--不包含上链</button>
      <br />

      <h3>获取用户所有的Activity</h3>
      <button onClick={getAllStories}>获取用户的所有Activity</button>
      {activityList.map((item, index) => {
        return (
          <div key={`${item.id}:${index}`}>
            {item.id} --- {item.content_id} ---{" "}
            {new Date(item.created).toLocaleString()}
          </div>
        );
      })}
      <h3>获取home feeds</h3>
      <button onClick={getHomeFeeds}>获取用户加入社区的feeds</button>

      <h3>获取top feeds</h3>
      <button onClick={getTopFeeds}>获取用户加入社区的feeds</button>

      <h3>获取用户 feeds</h3>
      <button onClick={getUserFeeds}>获取用户加入社区的feeds</button>

      <p>
        {feedList.map((item, index) => {
          const { view_id, title, content_id, text, community_id } = item;
          return (
            <div key={`${view_id}:${content_id}`}>
              {item.profile_id} --- {content_id}--{community_id}---{" "}
              {new Date(item.created).toLocaleString()} --- likeCount:
              {item.like_count}
              <button
                onClick={() => {
                  getFeedDetail(view_id);
                }}
              >
                详情
              </button>
              <button
                onClick={() => {
                  createActivityOnChainOnly(text);
                }}
              >
                单独发帖上链
              </button>
              <button
                onClick={() =>
                  handleDoLike(
                    item.profile_id,
                    view_id,
                    content_id,
                    ReactionKindEnum.LIKE
                  )
                }
              >
                点赞--链下
              </button>
              <button
                onClick={() =>
                  handleDoLikeOnChainOnly(
                    item.profile_id,
                    view_id,
                    content_id,
                    ReactionKindEnum.LIKE,
                    true
                  )
                }
              >
                点赞--单独上链
              </button>
              <button
                onClick={() =>
                  handleDoLikeOnChain(
                    item.profile_id,
                    view_id,
                    content_id,
                    ReactionKindEnum.LIKE,
                    true
                  )
                }
              >
                点赞--链上
              </button>
              <button
                onClick={() =>
                  handleUnDoLike(
                    item.profile_id,
                    view_id,
                    content_id,
                    ReactionKindEnum.LIKE
                  )
                }
              >
                取消点赞--链下
              </button>
              <button
                onClick={() =>
                  handleUnDoLikeOnChain(
                    item.profile_id,
                    view_id,
                    content_id,
                    ReactionKindEnum.LIKE,
                    true
                  )
                }
              >
                取消点赞--链上
              </button>
              <button
                onClick={() =>
                  handleComment(
                    item.profile_id,
                    view_id,
                    content_id,
                    ReactionKindEnum.COMMENT
                  )
                }
              >
                发表评论--链下
              </button>
              <button
                onClick={() =>
                  handleCommentOnChainOnly(
                    item.profile_id,
                    view_id,
                    content_id,
                    ContentTypeEnum.Comment,
                    ReactionKindEnum.COMMENT
                  )
                }
              >
                发表评论--单独上链
              </button>
              <button
                onClick={() =>
                  handleCommentOnChain(
                    item.profile_id,
                    view_id,
                    content_id,
                    ContentTypeEnum.Comment,
                    ReactionKindEnum.COMMENT
                  )
                }
              >
                发表评论--链上
              </button>
              <button
                onClick={() => {
                  handleGetCommentsList(view_id, ReactionKindEnum.COMMENT);
                }}
              >
                评论列表
              </button>
            </div>
          );
        })}
        {commentsList.map((item) => (
          <>
            <div>
              {item.id}--{item.content_id}
            </div>
            <button
              onClick={() => {
                handleGetCommentsList(item.id, ReactionKindEnum.REPLY);
              }}
            >
              回复列表
            </button>
            <button
              onClick={() =>
                handleReply(
                  item.profile_id,
                  item.id,
                  item.content_id,
                  ReactionKindEnum.REPLY
                )
              }
            >
              发表回复--链下
            </button>
            <button
              onClick={() =>
                handleReplyOnChain(
                  item.profile_id,
                  item.id,
                  item.content_id,
                  ContentTypeEnum.Comment,
                  ReactionKindEnum.REPLY
                )
              }
            >
              发表回复--链上
            </button>
            <button
              onClick={() =>
                handleReplyOnChainOnly(
                  item.profile_id,
                  item.id,
                  item.content_id,
                  ContentTypeEnum.Comment,
                  ReactionKindEnum.REPLY
                )
              }
            >
              发表回复--单独链上
            </button>
          </>
        ))}
      </p>
    </div>
  );
};
