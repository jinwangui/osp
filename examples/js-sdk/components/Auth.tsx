import React from "react";
import { Client } from "osp-client-js";

export function Auth({ client }: { client: Client }) {
  const connectWallet = async () => {
    const res = await client.wallet.connect();
    console.log("res", res);
  };

  const switchNetworks = async () => {
    const res = await client.wallet.switchNetworks();
  };

  const handleSignIn = async () => {
    const res = await client.auth.signIn();
    console.log("res", res);
  };

  const handleSignOut = () => {
    const res = client.auth.signOut();
  };
  const getContractAddress = () => {
    console.log(client.wallet.contractAddress);
  };

  return (
    <>
      <h2>Auth模块</h2>
      <h3>connectWallet</h3>

      <button onClick={connectWallet}>connectWallet</button>
      <h3>切换网络</h3>

      <button onClick={switchNetworks}>switchNetworks</button>
      <h3>登录</h3>

      <button onClick={handleSignIn}>signIn</button>

      <h3>退出</h3>

      <button onClick={handleSignOut}>signOut</button>

      <h3>获取合约地址</h3>

      <button onClick={getContractAddress}>获取合约地址</button>
    </>
  );
}
