import React, { useState } from "react";
import { Client, FollowModuleEnum, User as UserType } from "osp-client-js";

export const Community = ({ client }: { client: Client }) => {
  const [form, setForm] = useState({
    follow_module: {
      type: "HOLD_TOKEN_JOIN_MODULE",
      init: {
        native_fee_join_module: {
          recipient_address: "0x4ebFF36992Dd01EE5FDbc830eB7d3d1823Fe6f3F",
          amount_uint: `${1000000000000000000 * 1}`,
        },
      },
    },
    follow_nft_recived: "",
    follow_nft_currency: "",
    follow_nft_amount: "",
  });

  const [communityList, setCommunityList] = useState<UserType[]>([]);
  const [joinCommunityList, setJoinCommunityList] = useState<UserType[]>([]);
  const [createdCommunityList, setCreatedCommunityList] = useState<UserType[]>(
    []
  );

  const mintSlotNft = async () => {
    const res = await client.community.mintSlotNft({});
    console.log(res);
  };

  const checkSlotNFT = async () => {
    const {
      follow_module,
      handle,
      tokenId,
      follow_nft_amount,
      follow_nft_recived,
      ...rest
    } = form;
    const res = await client.community.checkSlotNFT(tokenId);
    console.log(res);
  };
  const createCommunity = async () => {
    console.log(form, "formform");
    const {
      follow_module,
      handle,
      tokenId,
      bio,
      display_name,
      logo,
      banner,
      follow_nft_amount,
      follow_nft_recived,
      tags,
      ...rest
    } = form;

    if (!handle) return alert("please input handle");
    if (!tokenId) return alert("please input tokenId");

    const res = await client.community.create({
      handle,
      tokenId,
      display_name: display_name,
      desc: bio,
      logo: logo,
      banner: banner,
      tags: tags.split(","),
      template: "1",
      join_module: {
        type: "NATIVE_FEE_JOIN_MODULE",
        init: {
          native_fee_join_module: {
            recipient_address: "0x4ebFF36992Dd01EE5FDbc830eB7d3d1823Fe6f3F",
            amount_uint: `${1000000000000000000 * 1}`,
          },
        },
      },
    });
    console.log("res", res);
  };
  const getAllCommunity = async () => {
    const query = {
      ranking: "ORDER",
      limit: 20,
      next_token: "",
      withProfile: true,
      tag_id: "",
    };
    const { data } = await client.community.getAllCommunity(query);
    setCommunityList(data.rows);
  };
  const getAllCreated = async () => {
    const query = {
      ranking: "ORDER",
      limit: 20,
      next_token: "",
      withProfile: true,
    };
    const { data } = await client.community.getAllCreated(query);
    setCreatedCommunityList(data.rows);
  };

  const getAllJoin = async () => {
    const profileId = client.profile.profileId;
    const query = {
      ranking: "ORDER",
      limit: 20,
      next_token: "",
      withProfile: true,
    };
    const { data } = await client.community.getAllJoin(profileId, query);
    setJoinCommunityList(data.rows);
  };

  const get = async (communityId) => {
    const data = await client.community.get(communityId);
    console.log(data);
  };
  const getTags = async () => {
    const data = await client.community.getTags();
    console.log(data);
  };

  const getCommunityNFT = async (tokenId) => {
    const data = await client.nft.getCommunityNFT(tokenId);
    console.log(data);
  };

  const getByHandle = async (handle) => {
    const data = await client.community.getByHandle(handle);
    console.log(data);
  };
  const bindDomain = async (communityId, domain = "abcd.penuel.cn") => {
    const data = await client.community.updateDomain(communityId, { domain });
    console.log(data);
  };
  const update = async (communityId) => {
    const res = await client.community.update(
      communityId,

      {
        fields: {
          desc: Math.random(),
          join_module: {
            type: "NATIVE_FEE_JOIN_MODULE",
            init: {
              native_fee_join_module: {
                recipient_address: "0x4ebFF36992Dd01EE5FDbc830eB7d3d1823Fe6f3F",
                amount_uint: `${1000000000000000000 * 1}`,
              },
            },
          },
        }

      }
    );
    console.log("ppppp", res);
  };
  const join = async (communityId) => {
    const res = await client.community.join({
      id: communityId,
    });
    console.log("ppppp", res);
  };

  const formChange = (key, value) => setForm({ ...form, [key]: value });

  const typeChange = (e) => {
    if (!(e.target.value === FollowModuleEnum.FeeFollowModule)) {
      formChange("follow_nft_currency", undefined);
      formChange("follow_nft_amount", undefined);
    }
    formChange("follow_module", e.target.value);
  };

  const checkoutHandle = () => {
    client.community.getByHandle(form.handle, false);
  };
  return (
    <div>
      <h2>Community模块</h2>
      <h3>创建Community</h3>
      生成slotnft
      <button onClick={mintSlotNft}>mintSlotNft</button>
      <br />
      类型:
      <br />
      handle: <input onChange={(e) => formChange("handle", e.target.value)} />
      用户SlotNFTTokenID::{" "}
      <input onChange={(e) => formChange("tokenId", e.target.value)} />
      <br />
      display_name:{" "}
      <input onChange={(e) => formChange("display_name", e.target.value)} />
      <br />
      简介: <input onChange={(e) => formChange("bio", e.target.value)} />
      <br />
      tags: <input onChange={(e) => formChange("tags", e.target.value)} />
      <br />
      Logo: <input onChange={(e) => formChange("logo", e.target.value)} />
      <br />
      Banner: <input onChange={(e) => formChange("banner", e.target.value)} />
      <br />
      <p>注意：头像为必传， 用户名不能少于5位</p>
      <button onClick={createCommunity}>createCommunity</button>
      <button onClick={checkSlotNFT}>checkSlotNFT</button>
      <button onClick={checkoutHandle}>检验handle是否唯一</button>
      <br />
      <h3>获取社区tags</h3>
      <button onClick={getTags}>获取社区tags</button>
      <h3>获取所有的community</h3>
      <button onClick={getAllCommunity}>获取所有的community</button>
      <br />
      {communityList.map((i) => (
        <>
          <br />
          <span>
            {i.id} {i.handle}
          </span>
          <button onClick={() => get(i.id)}>获取community详情</button>
          <button onClick={() => getCommunityNFT(i.id)}>
            获取community NFT详情
          </button>

          <button onClick={() => update(i.id)}>更新community</button>

          <button onClick={() => getByHandle(i.handle)}>
            通过handle获取community信息
          </button>
          <button onClick={() => join(i.id)}>加入community</button>
          <button onClick={() => bindDomain(i.id)}>绑定域名</button>
        </>
      ))}
      <h3>获取用户创建的所有的community</h3>
      <button onClick={getAllCreated}>获取用户创建的所有的community</button>
      {createdCommunityList.map((i) => (
        <>
          <br />
          <span>
            {i.id} {i.handle}
          </span>
          <button onClick={() => get(i.id)}>获取community详情</button>

          <button onClick={() => update(i.id)}>更新community</button>
          <button onClick={() => join(i.id)}>加入community</button>
        </>
      ))}
      <br />
      <h3>获取用户加入的所有的community</h3>
      <button onClick={getAllJoin}>获取用户加入的所有的community</button>
      {joinCommunityList.map((i) => (
        <>
          <br />
          <span>
            {i.id} {i.handle}
          </span>
          <button onClick={() => get(i.id)}>获取community详情</button>

          <button onClick={() => update(i.id)}>更新community</button>
        </>
      ))}
    </div>
  );
};
