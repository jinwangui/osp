// import { useEffect, useState } from "react";

export const User = ({
  address,
  apiClient,
}: {
  address?: string;
  apiClient?: any;
}) => {
  const createProfiles = async () => {
    const res = await apiClient.users.createUser({
      follow_module: {
        type: "ProfileFollowModule",
        init: {
          // "FeeFollowModule":{
          //   "present": true,
          // }
        },
      },
      follow_nft_uri: "",
      handle: "test333",
      bio: "hi",
      avatar:
        "ipfs://bafkreifb32me5uvvu7nprplw7ruuvjzreswrbqn4wiadmxghc6tdbbak7a",
      cover_picture:
        "ipfs://bafkreifb32me5uvvu7nprplw7ruuvjzreswrbqn4wiadmxghc6tdbbak7a",
    });
  };
  const getAllProfiles = async () => {
    const res = await apiClient.profile.getUsers({ address });
    console.log("ppppp", res);
  };

  const getProfile = async () => {
    const res = await apiClient.profile.getUser("0xf7");
    console.log("ppppp", res);
  };

  return (
    <div>
      <h2>User模块</h2>
      <h3>创建用户的profile</h3>
      <button onClick={createProfiles}>创建用户的profile</button>
      <br />
      <h3>获取用户所有的profile</h3>
      <button onClick={getAllProfiles}>获取用户的所有profile</button>
      <br />
      <h3>查询用户某一个profile</h3>
      <button onClick={getProfile}>获取用户的profile</button>
    </div>
  );
};
