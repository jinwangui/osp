export const Community = ({
  address,
  apiClient,
}: {
  address?: string;
  apiClient?: any;
}) => {
  const listCommunity = async () => {
    const res = await apiClient.communities.listCommunity({
      ranking: "ORDER",
      limit: 20,
      next_token: "",
      withProfile: true,
    });
  };

  const getCommunityById = async () => {
    const res = await apiClient.communities.getCommunityById("0xb");
    console.log("ppppp", res);
  };

  const updateCommunityById = async () => {
    const res = await apiClient.communities.updateCommunityById("0xb", {
      display_name: "luomei1111",
    });
    console.log("ppppp", res);
  };

  const getCommunityByHandle = async () => {
    const res = await apiClient.communities.getCommunityByHandle(
      "community_handle_name_luomei15"
    );
    console.log("ppppp", res);
  };

  return (
    <div>
      <h2>Community模块</h2>

      <h3>获取所有Community</h3>
      <button onClick={listCommunity}>获取所有Community</button>

      <br />
      <h3>getCommunityById</h3>
      <button onClick={getCommunityById}>通过id获取</button>
      <br />
      <h3>updateCommunityById</h3>
      <button onClick={updateCommunityById}>修改Community</button>
      <br />
      <h3>getCommunityByHandle</h3>
      <button onClick={getCommunityByHandle}>通过handle获取Community</button>
    </div>
  );
};
