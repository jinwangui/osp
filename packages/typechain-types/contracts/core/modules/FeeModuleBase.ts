/* Autogenerated file. Do not edit manually. */
/* tslint:disable */
/* eslint-disable */
import type {
  BaseContract,
  BigNumber,
  BytesLike,
  CallOverrides,
  PopulatedTransaction,
  Signer,
  utils,
} from "ethers";
import type { FunctionFragment, Result } from "@ethersproject/abi";
import type { Listener, Provider } from "@ethersproject/providers";
import type {
  TypedEventFilter,
  TypedEvent,
  TypedListener,
  OnEvent,
  PromiseOrValue,
} from "../../../common";

export interface FeeModuleBaseInterface extends utils.Interface {
  functions: {
    "OSP()": FunctionFragment;
  };

  getFunction(nameOrSignatureOrTopic: "OSP"): FunctionFragment;

  encodeFunctionData(functionFragment: "OSP", values?: undefined): string;

  decodeFunctionResult(functionFragment: "OSP", data: BytesLike): Result;

  events: {};
}

export interface FeeModuleBase extends BaseContract {
  connect(signerOrProvider: Signer | Provider | string): this;
  attach(addressOrName: string): this;
  deployed(): Promise<this>;

  interface: FeeModuleBaseInterface;

  queryFilter<TEvent extends TypedEvent>(
    event: TypedEventFilter<TEvent>,
    fromBlockOrBlockhash?: string | number | undefined,
    toBlock?: string | number | undefined
  ): Promise<Array<TEvent>>;

  listeners<TEvent extends TypedEvent>(
    eventFilter?: TypedEventFilter<TEvent>
  ): Array<TypedListener<TEvent>>;
  listeners(eventName?: string): Array<Listener>;
  removeAllListeners<TEvent extends TypedEvent>(
    eventFilter: TypedEventFilter<TEvent>
  ): this;
  removeAllListeners(eventName?: string): this;
  off: OnEvent<this>;
  on: OnEvent<this>;
  once: OnEvent<this>;
  removeListener: OnEvent<this>;

  functions: {
    OSP(overrides?: CallOverrides): Promise<[string]>;
  };

  OSP(overrides?: CallOverrides): Promise<string>;

  callStatic: {
    OSP(overrides?: CallOverrides): Promise<string>;
  };

  filters: {};

  estimateGas: {
    OSP(overrides?: CallOverrides): Promise<BigNumber>;
  };

  populateTransaction: {
    OSP(overrides?: CallOverrides): Promise<PopulatedTransaction>;
  };
}
