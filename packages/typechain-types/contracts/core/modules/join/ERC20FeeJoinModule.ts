/* Autogenerated file. Do not edit manually. */
/* tslint:disable */
/* eslint-disable */
import type {
  BaseContract,
  BigNumber,
  BigNumberish,
  BytesLike,
  CallOverrides,
  ContractTransaction,
  Overrides,
  PayableOverrides,
  PopulatedTransaction,
  Signer,
  utils,
} from "ethers";
import type { FunctionFragment, Result } from "@ethersproject/abi";
import type { Listener, Provider } from "@ethersproject/providers";
import type {
  TypedEventFilter,
  TypedEvent,
  TypedListener,
  OnEvent,
  PromiseOrValue,
} from "../../../../common";

export type CommunityDataStruct = {
  currency: PromiseOrValue<string>;
  amount: PromiseOrValue<BigNumberish>;
  recipient: PromiseOrValue<string>;
};

export type CommunityDataStructOutput = [string, BigNumber, string] & {
  currency: string;
  amount: BigNumber;
  recipient: string;
};

export interface ERC20FeeJoinModuleInterface extends utils.Interface {
  functions: {
    "OSP()": FunctionFragment;
    "getCommunityData(uint256)": FunctionFragment;
    "initializeCommunityJoinModule(uint256,bytes)": FunctionFragment;
    "processJoin(address,uint256,bytes)": FunctionFragment;
  };

  getFunction(
    nameOrSignatureOrTopic:
      | "OSP"
      | "getCommunityData"
      | "initializeCommunityJoinModule"
      | "processJoin"
  ): FunctionFragment;

  encodeFunctionData(functionFragment: "OSP", values?: undefined): string;
  encodeFunctionData(
    functionFragment: "getCommunityData",
    values: [PromiseOrValue<BigNumberish>]
  ): string;
  encodeFunctionData(
    functionFragment: "initializeCommunityJoinModule",
    values: [PromiseOrValue<BigNumberish>, PromiseOrValue<BytesLike>]
  ): string;
  encodeFunctionData(
    functionFragment: "processJoin",
    values: [
      PromiseOrValue<string>,
      PromiseOrValue<BigNumberish>,
      PromiseOrValue<BytesLike>
    ]
  ): string;

  decodeFunctionResult(functionFragment: "OSP", data: BytesLike): Result;
  decodeFunctionResult(
    functionFragment: "getCommunityData",
    data: BytesLike
  ): Result;
  decodeFunctionResult(
    functionFragment: "initializeCommunityJoinModule",
    data: BytesLike
  ): Result;
  decodeFunctionResult(
    functionFragment: "processJoin",
    data: BytesLike
  ): Result;

  events: {};
}

export interface ERC20FeeJoinModule extends BaseContract {
  connect(signerOrProvider: Signer | Provider | string): this;
  attach(addressOrName: string): this;
  deployed(): Promise<this>;

  interface: ERC20FeeJoinModuleInterface;

  queryFilter<TEvent extends TypedEvent>(
    event: TypedEventFilter<TEvent>,
    fromBlockOrBlockhash?: string | number | undefined,
    toBlock?: string | number | undefined
  ): Promise<Array<TEvent>>;

  listeners<TEvent extends TypedEvent>(
    eventFilter?: TypedEventFilter<TEvent>
  ): Array<TypedListener<TEvent>>;
  listeners(eventName?: string): Array<Listener>;
  removeAllListeners<TEvent extends TypedEvent>(
    eventFilter: TypedEventFilter<TEvent>
  ): this;
  removeAllListeners(eventName?: string): this;
  off: OnEvent<this>;
  on: OnEvent<this>;
  once: OnEvent<this>;
  removeListener: OnEvent<this>;

  functions: {
    OSP(overrides?: CallOverrides): Promise<[string]>;

    getCommunityData(
      communityId: PromiseOrValue<BigNumberish>,
      overrides?: CallOverrides
    ): Promise<[CommunityDataStructOutput]>;

    initializeCommunityJoinModule(
      communityId: PromiseOrValue<BigNumberish>,
      data: PromiseOrValue<BytesLike>,
      overrides?: Overrides & { from?: PromiseOrValue<string> }
    ): Promise<ContractTransaction>;

    processJoin(
      follower: PromiseOrValue<string>,
      communityId: PromiseOrValue<BigNumberish>,
      data: PromiseOrValue<BytesLike>,
      overrides?: PayableOverrides & { from?: PromiseOrValue<string> }
    ): Promise<ContractTransaction>;
  };

  OSP(overrides?: CallOverrides): Promise<string>;

  getCommunityData(
    communityId: PromiseOrValue<BigNumberish>,
    overrides?: CallOverrides
  ): Promise<CommunityDataStructOutput>;

  initializeCommunityJoinModule(
    communityId: PromiseOrValue<BigNumberish>,
    data: PromiseOrValue<BytesLike>,
    overrides?: Overrides & { from?: PromiseOrValue<string> }
  ): Promise<ContractTransaction>;

  processJoin(
    follower: PromiseOrValue<string>,
    communityId: PromiseOrValue<BigNumberish>,
    data: PromiseOrValue<BytesLike>,
    overrides?: PayableOverrides & { from?: PromiseOrValue<string> }
  ): Promise<ContractTransaction>;

  callStatic: {
    OSP(overrides?: CallOverrides): Promise<string>;

    getCommunityData(
      communityId: PromiseOrValue<BigNumberish>,
      overrides?: CallOverrides
    ): Promise<CommunityDataStructOutput>;

    initializeCommunityJoinModule(
      communityId: PromiseOrValue<BigNumberish>,
      data: PromiseOrValue<BytesLike>,
      overrides?: CallOverrides
    ): Promise<void>;

    processJoin(
      follower: PromiseOrValue<string>,
      communityId: PromiseOrValue<BigNumberish>,
      data: PromiseOrValue<BytesLike>,
      overrides?: CallOverrides
    ): Promise<void>;
  };

  filters: {};

  estimateGas: {
    OSP(overrides?: CallOverrides): Promise<BigNumber>;

    getCommunityData(
      communityId: PromiseOrValue<BigNumberish>,
      overrides?: CallOverrides
    ): Promise<BigNumber>;

    initializeCommunityJoinModule(
      communityId: PromiseOrValue<BigNumberish>,
      data: PromiseOrValue<BytesLike>,
      overrides?: Overrides & { from?: PromiseOrValue<string> }
    ): Promise<BigNumber>;

    processJoin(
      follower: PromiseOrValue<string>,
      communityId: PromiseOrValue<BigNumberish>,
      data: PromiseOrValue<BytesLike>,
      overrides?: PayableOverrides & { from?: PromiseOrValue<string> }
    ): Promise<BigNumber>;
  };

  populateTransaction: {
    OSP(overrides?: CallOverrides): Promise<PopulatedTransaction>;

    getCommunityData(
      communityId: PromiseOrValue<BigNumberish>,
      overrides?: CallOverrides
    ): Promise<PopulatedTransaction>;

    initializeCommunityJoinModule(
      communityId: PromiseOrValue<BigNumberish>,
      data: PromiseOrValue<BytesLike>,
      overrides?: Overrides & { from?: PromiseOrValue<string> }
    ): Promise<PopulatedTransaction>;

    processJoin(
      follower: PromiseOrValue<string>,
      communityId: PromiseOrValue<BigNumberish>,
      data: PromiseOrValue<BytesLike>,
      overrides?: PayableOverrides & { from?: PromiseOrValue<string> }
    ): Promise<PopulatedTransaction>;
  };
}
