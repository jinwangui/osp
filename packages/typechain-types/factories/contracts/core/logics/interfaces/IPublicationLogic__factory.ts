/* Autogenerated file. Do not edit manually. */
/* tslint:disable */
/* eslint-disable */

import { Contract, Signer, utils } from "ethers";
import type { Provider } from "@ethersproject/providers";
import type {
  IPublicationLogic,
  IPublicationLogicInterface,
} from "../../../../../contracts/core/logics/interfaces/IPublicationLogic";

const _abi = [
  {
    inputs: [
      {
        internalType: "uint256",
        name: "profileId",
        type: "uint256",
      },
      {
        internalType: "uint256",
        name: "contentId",
        type: "uint256",
      },
    ],
    name: "getCollectModule",
    outputs: [
      {
        internalType: "address",
        name: "",
        type: "address",
      },
    ],
    stateMutability: "view",
    type: "function",
  },
  {
    inputs: [
      {
        internalType: "uint256",
        name: "profileId",
        type: "uint256",
      },
      {
        internalType: "uint256",
        name: "contentId",
        type: "uint256",
      },
    ],
    name: "getCollectNFT",
    outputs: [
      {
        internalType: "address",
        name: "",
        type: "address",
      },
    ],
    stateMutability: "view",
    type: "function",
  },
  {
    inputs: [
      {
        internalType: "uint256",
        name: "profileId",
        type: "uint256",
      },
      {
        internalType: "uint256",
        name: "contentId",
        type: "uint256",
      },
    ],
    name: "getContent",
    outputs: [
      {
        components: [
          {
            internalType: "enum OspDataTypes.ContentType",
            name: "contentType",
            type: "uint8",
          },
          {
            internalType: "uint256",
            name: "communityId",
            type: "uint256",
          },
          {
            internalType: "uint256",
            name: "referencedProfileId",
            type: "uint256",
          },
          {
            internalType: "uint256",
            name: "referencedContentId",
            type: "uint256",
          },
          {
            internalType: "string",
            name: "contentURI",
            type: "string",
          },
          {
            internalType: "address",
            name: "referenceModule",
            type: "address",
          },
          {
            internalType: "address",
            name: "collectModule",
            type: "address",
          },
          {
            internalType: "address",
            name: "collectNFT",
            type: "address",
          },
        ],
        internalType: "struct OspDataTypes.ContentStruct",
        name: "",
        type: "tuple",
      },
    ],
    stateMutability: "view",
    type: "function",
  },
  {
    inputs: [
      {
        internalType: "uint256",
        name: "profileId",
        type: "uint256",
      },
    ],
    name: "getContentCount",
    outputs: [
      {
        internalType: "uint256",
        name: "",
        type: "uint256",
      },
    ],
    stateMutability: "view",
    type: "function",
  },
  {
    inputs: [
      {
        internalType: "uint256",
        name: "profileId",
        type: "uint256",
      },
      {
        internalType: "uint256",
        name: "contentId",
        type: "uint256",
      },
    ],
    name: "getContentPointer",
    outputs: [
      {
        internalType: "uint256",
        name: "",
        type: "uint256",
      },
      {
        internalType: "uint256",
        name: "",
        type: "uint256",
      },
    ],
    stateMutability: "view",
    type: "function",
  },
  {
    inputs: [
      {
        internalType: "uint256",
        name: "profileId",
        type: "uint256",
      },
      {
        internalType: "uint256",
        name: "contentId",
        type: "uint256",
      },
    ],
    name: "getContentType",
    outputs: [
      {
        internalType: "enum OspDataTypes.ContentType",
        name: "",
        type: "uint8",
      },
    ],
    stateMutability: "view",
    type: "function",
  },
  {
    inputs: [
      {
        internalType: "uint256",
        name: "profileId",
        type: "uint256",
      },
      {
        internalType: "uint256",
        name: "contentId",
        type: "uint256",
      },
    ],
    name: "getContentURI",
    outputs: [
      {
        internalType: "string",
        name: "",
        type: "string",
      },
    ],
    stateMutability: "view",
    type: "function",
  },
  {
    inputs: [
      {
        internalType: "uint256",
        name: "profileId",
        type: "uint256",
      },
      {
        internalType: "uint256",
        name: "contentId",
        type: "uint256",
      },
    ],
    name: "getReferenceModule",
    outputs: [
      {
        internalType: "address",
        name: "",
        type: "address",
      },
    ],
    stateMutability: "view",
    type: "function",
  },
  {
    inputs: [
      {
        components: [
          {
            internalType: "uint256",
            name: "profileId",
            type: "uint256",
          },
          {
            internalType: "uint256",
            name: "communityId",
            type: "uint256",
          },
          {
            internalType: "string",
            name: "contentURI",
            type: "string",
          },
          {
            internalType: "address",
            name: "collectModule",
            type: "address",
          },
          {
            internalType: "bytes",
            name: "collectModuleInitData",
            type: "bytes",
          },
          {
            internalType: "address",
            name: "referenceModule",
            type: "address",
          },
          {
            internalType: "bytes",
            name: "referenceModuleInitData",
            type: "bytes",
          },
        ],
        internalType: "struct OspDataTypes.PostData",
        name: "vars",
        type: "tuple",
      },
    ],
    name: "post",
    outputs: [
      {
        internalType: "uint256",
        name: "",
        type: "uint256",
      },
    ],
    stateMutability: "nonpayable",
    type: "function",
  },
  {
    inputs: [
      {
        components: [
          {
            internalType: "uint256",
            name: "profileId",
            type: "uint256",
          },
          {
            internalType: "uint256",
            name: "communityId",
            type: "uint256",
          },
          {
            internalType: "string",
            name: "contentURI",
            type: "string",
          },
          {
            internalType: "address",
            name: "collectModule",
            type: "address",
          },
          {
            internalType: "bytes",
            name: "collectModuleInitData",
            type: "bytes",
          },
          {
            internalType: "address",
            name: "referenceModule",
            type: "address",
          },
          {
            internalType: "bytes",
            name: "referenceModuleInitData",
            type: "bytes",
          },
          {
            components: [
              {
                internalType: "uint8",
                name: "v",
                type: "uint8",
              },
              {
                internalType: "bytes32",
                name: "r",
                type: "bytes32",
              },
              {
                internalType: "bytes32",
                name: "s",
                type: "bytes32",
              },
              {
                internalType: "uint256",
                name: "deadline",
                type: "uint256",
              },
            ],
            internalType: "struct OspDataTypes.EIP712Signature",
            name: "sig",
            type: "tuple",
          },
        ],
        internalType: "struct OspDataTypes.PostWithSigData",
        name: "vars",
        type: "tuple",
      },
    ],
    name: "postWithSig",
    outputs: [
      {
        internalType: "uint256",
        name: "",
        type: "uint256",
      },
    ],
    stateMutability: "nonpayable",
    type: "function",
  },
] as const;

export class IPublicationLogic__factory {
  static readonly abi = _abi;
  static createInterface(): IPublicationLogicInterface {
    return new utils.Interface(_abi) as IPublicationLogicInterface;
  }
  static connect(
    address: string,
    signerOrProvider: Signer | Provider
  ): IPublicationLogic {
    return new Contract(address, _abi, signerOrProvider) as IPublicationLogic;
  }
}
