/* Autogenerated file. Do not edit manually. */
/* tslint:disable */
/* eslint-disable */

import { Contract, Signer, utils } from "ethers";
import type { Provider } from "@ethersproject/providers";
import type {
  IReactionLogic,
  IReactionLogicInterface,
} from "../../../../../contracts/core/logics/interfaces/IReactionLogic";

const _abi = [
  {
    inputs: [
      {
        components: [
          {
            internalType: "enum OspDataTypes.ContentType",
            name: "reactionType",
            type: "uint8",
          },
          {
            internalType: "uint256",
            name: "profileId",
            type: "uint256",
          },
          {
            internalType: "string",
            name: "contentURI",
            type: "string",
          },
          {
            internalType: "uint256",
            name: "referencedProfileId",
            type: "uint256",
          },
          {
            internalType: "uint256",
            name: "referencedContentId",
            type: "uint256",
          },
          {
            internalType: "bytes",
            name: "referenceModuleData",
            type: "bytes",
          },
          {
            internalType: "address",
            name: "referenceModule",
            type: "address",
          },
          {
            internalType: "bytes",
            name: "referenceModuleInitData",
            type: "bytes",
          },
        ],
        internalType: "struct OspDataTypes.ContentReactionData",
        name: "vars",
        type: "tuple",
      },
    ],
    name: "addContentReaction",
    outputs: [
      {
        internalType: "uint256",
        name: "",
        type: "uint256",
      },
    ],
    stateMutability: "nonpayable",
    type: "function",
  },
  {
    inputs: [
      {
        components: [
          {
            internalType: "enum OspDataTypes.ContentType",
            name: "reactionType",
            type: "uint8",
          },
          {
            internalType: "uint256",
            name: "profileId",
            type: "uint256",
          },
          {
            internalType: "string",
            name: "contentURI",
            type: "string",
          },
          {
            internalType: "uint256",
            name: "referencedProfileId",
            type: "uint256",
          },
          {
            internalType: "uint256",
            name: "referencedContentId",
            type: "uint256",
          },
          {
            internalType: "bytes",
            name: "referenceModuleData",
            type: "bytes",
          },
          {
            internalType: "address",
            name: "referenceModule",
            type: "address",
          },
          {
            internalType: "bytes",
            name: "referenceModuleInitData",
            type: "bytes",
          },
          {
            components: [
              {
                internalType: "uint8",
                name: "v",
                type: "uint8",
              },
              {
                internalType: "bytes32",
                name: "r",
                type: "bytes32",
              },
              {
                internalType: "bytes32",
                name: "s",
                type: "bytes32",
              },
              {
                internalType: "uint256",
                name: "deadline",
                type: "uint256",
              },
            ],
            internalType: "struct OspDataTypes.EIP712Signature",
            name: "sig",
            type: "tuple",
          },
        ],
        internalType: "struct OspDataTypes.ContentReactionWithSigData",
        name: "vars",
        type: "tuple",
      },
    ],
    name: "addContentReactionWithSig",
    outputs: [
      {
        internalType: "uint256",
        name: "",
        type: "uint256",
      },
    ],
    stateMutability: "nonpayable",
    type: "function",
  },
  {
    inputs: [
      {
        components: [
          {
            internalType: "enum OspDataTypes.ReactionType",
            name: "reactionType",
            type: "uint8",
          },
          {
            internalType: "uint256",
            name: "profileId",
            type: "uint256",
          },
          {
            internalType: "uint256",
            name: "referencedProfileId",
            type: "uint256",
          },
          {
            internalType: "uint256",
            name: "referencedContentId",
            type: "uint256",
          },
          {
            internalType: "bytes",
            name: "referenceModuleData",
            type: "bytes",
          },
          {
            internalType: "bytes",
            name: "data",
            type: "bytes",
          },
        ],
        internalType: "struct OspDataTypes.ReactionData",
        name: "vars",
        type: "tuple",
      },
    ],
    name: "addReaction",
    outputs: [],
    stateMutability: "nonpayable",
    type: "function",
  },
  {
    inputs: [
      {
        components: [
          {
            internalType: "enum OspDataTypes.ReactionType",
            name: "reactionType",
            type: "uint8",
          },
          {
            internalType: "uint256",
            name: "profileId",
            type: "uint256",
          },
          {
            internalType: "uint256",
            name: "referencedProfileId",
            type: "uint256",
          },
          {
            internalType: "uint256",
            name: "referencedContentId",
            type: "uint256",
          },
          {
            internalType: "bytes",
            name: "referenceModuleData",
            type: "bytes",
          },
          {
            internalType: "bytes",
            name: "data",
            type: "bytes",
          },
          {
            components: [
              {
                internalType: "uint8",
                name: "v",
                type: "uint8",
              },
              {
                internalType: "bytes32",
                name: "r",
                type: "bytes32",
              },
              {
                internalType: "bytes32",
                name: "s",
                type: "bytes32",
              },
              {
                internalType: "uint256",
                name: "deadline",
                type: "uint256",
              },
            ],
            internalType: "struct OspDataTypes.EIP712Signature",
            name: "sig",
            type: "tuple",
          },
        ],
        internalType: "struct OspDataTypes.ReactionWithSigData",
        name: "vars",
        type: "tuple",
      },
    ],
    name: "addReactionWithSig",
    outputs: [],
    stateMutability: "nonpayable",
    type: "function",
  },
  {
    inputs: [
      {
        internalType: "uint256",
        name: "profileId",
        type: "uint256",
      },
      {
        internalType: "uint256",
        name: "contentId",
        type: "uint256",
      },
      {
        internalType: "bytes",
        name: "data",
        type: "bytes",
      },
    ],
    name: "collect",
    outputs: [
      {
        internalType: "uint256",
        name: "",
        type: "uint256",
      },
    ],
    stateMutability: "nonpayable",
    type: "function",
  },
  {
    inputs: [
      {
        components: [
          {
            internalType: "address",
            name: "collector",
            type: "address",
          },
          {
            internalType: "uint256",
            name: "profileId",
            type: "uint256",
          },
          {
            internalType: "uint256",
            name: "contentId",
            type: "uint256",
          },
          {
            internalType: "bytes",
            name: "data",
            type: "bytes",
          },
          {
            components: [
              {
                internalType: "uint8",
                name: "v",
                type: "uint8",
              },
              {
                internalType: "bytes32",
                name: "r",
                type: "bytes32",
              },
              {
                internalType: "bytes32",
                name: "s",
                type: "bytes32",
              },
              {
                internalType: "uint256",
                name: "deadline",
                type: "uint256",
              },
            ],
            internalType: "struct OspDataTypes.EIP712Signature",
            name: "sig",
            type: "tuple",
          },
        ],
        internalType: "struct OspDataTypes.CollectWithSigData",
        name: "vars",
        type: "tuple",
      },
    ],
    name: "collectWithSig",
    outputs: [
      {
        internalType: "uint256",
        name: "",
        type: "uint256",
      },
    ],
    stateMutability: "nonpayable",
    type: "function",
  },
  {
    inputs: [
      {
        internalType: "uint256",
        name: "profileId",
        type: "uint256",
      },
      {
        internalType: "uint256",
        name: "contentId",
        type: "uint256",
      },
      {
        internalType: "uint256",
        name: "collectNFTId",
        type: "uint256",
      },
      {
        internalType: "address",
        name: "from",
        type: "address",
      },
      {
        internalType: "address",
        name: "to",
        type: "address",
      },
    ],
    name: "emitCollectNFTTransferEvent",
    outputs: [],
    stateMutability: "nonpayable",
    type: "function",
  },
  {
    inputs: [
      {
        internalType: "enum OspDataTypes.ReactionType",
        name: "reactionType",
        type: "uint8",
      },
      {
        internalType: "uint256",
        name: "profileId",
        type: "uint256",
      },
      {
        internalType: "uint256",
        name: "referencedProfileId",
        type: "uint256",
      },
      {
        internalType: "uint256",
        name: "referencedContentId",
        type: "uint256",
      },
    ],
    name: "getReaction",
    outputs: [
      {
        internalType: "bytes",
        name: "",
        type: "bytes",
      },
    ],
    stateMutability: "view",
    type: "function",
  },
] as const;

export class IReactionLogic__factory {
  static readonly abi = _abi;
  static createInterface(): IReactionLogicInterface {
    return new utils.Interface(_abi) as IReactionLogicInterface;
  }
  static connect(
    address: string,
    signerOrProvider: Signer | Provider
  ): IReactionLogic {
    return new Contract(address, _abi, signerOrProvider) as IReactionLogic;
  }
}
