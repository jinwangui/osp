/* Autogenerated file. Do not edit manually. */
/* tslint:disable */
/* eslint-disable */
import { Signer, utils, Contract, ContractFactory, Overrides } from "ethers";
import type { Provider, TransactionRequest } from "@ethersproject/providers";
import type { PromiseOrValue } from "../../../common";
import type {
  MockFollowModule,
  MockFollowModuleInterface,
} from "../../../contracts/mocks/MockFollowModule";

const _abi = [
  {
    inputs: [
      {
        internalType: "uint256",
        name: "profileId",
        type: "uint256",
      },
      {
        internalType: "address",
        name: "from",
        type: "address",
      },
      {
        internalType: "address",
        name: "to",
        type: "address",
      },
      {
        internalType: "uint256",
        name: "FollowSBTTokenId",
        type: "uint256",
      },
    ],
    name: "followModuleTransferHook",
    outputs: [],
    stateMutability: "nonpayable",
    type: "function",
  },
  {
    inputs: [
      {
        internalType: "uint256",
        name: "profileId",
        type: "uint256",
      },
      {
        internalType: "bytes",
        name: "data",
        type: "bytes",
      },
    ],
    name: "initializeFollowModule",
    outputs: [],
    stateMutability: "pure",
    type: "function",
  },
  {
    inputs: [
      {
        internalType: "address",
        name: "follower",
        type: "address",
      },
      {
        internalType: "uint256",
        name: "profileId",
        type: "uint256",
      },
      {
        internalType: "bytes",
        name: "data",
        type: "bytes",
      },
    ],
    name: "processFollow",
    outputs: [],
    stateMutability: "nonpayable",
    type: "function",
  },
] as const;

const _bytecode =
  "0x608060405234801561001057600080fd5b5061027a806100206000396000f3fe608060405234801561001057600080fd5b50600436106100415760003560e01c80630e096ae1146100465780633cb22cc41461005c5780639713958a1461006a575b600080fd5b61005a610054366004610141565b50505050565b005b61005a61005436600461019b565b61005a6100783660046101df565b60006100868284018461022b565b9050806001146100545760405162461bcd60e51b815260206004820152601960248201527f4d6f636b466f6c6c6f774d6f64756c653a20696e76616c696400000000000000604482015260640160405180910390fd5b80356001600160a01b03811681146100f357600080fd5b919050565b60008083601f84011261010a57600080fd5b50813567ffffffffffffffff81111561012257600080fd5b60208301915083602082850101111561013a57600080fd5b9250929050565b6000806000806060858703121561015757600080fd5b610160856100dc565b935060208501359250604085013567ffffffffffffffff81111561018357600080fd5b61018f878288016100f8565b95989497509550505050565b600080600080608085870312156101b157600080fd5b843593506101c1602086016100dc565b92506101cf604086016100dc565b9396929550929360600135925050565b6000806000604084860312156101f457600080fd5b83359250602084013567ffffffffffffffff81111561021257600080fd5b61021e868287016100f8565b9497909650939450505050565b60006020828403121561023d57600080fd5b503591905056fea2646970667358221220e3b45d5cfa330db215a1ab399ac8f8d7d8cdf004fd62c8fc19b8d032f16d81c264736f6c63430008120033";

type MockFollowModuleConstructorParams =
  | [signer?: Signer]
  | ConstructorParameters<typeof ContractFactory>;

const isSuperArgs = (
  xs: MockFollowModuleConstructorParams
): xs is ConstructorParameters<typeof ContractFactory> => xs.length > 1;

export class MockFollowModule__factory extends ContractFactory {
  constructor(...args: MockFollowModuleConstructorParams) {
    if (isSuperArgs(args)) {
      super(...args);
    } else {
      super(_abi, _bytecode, args[0]);
    }
  }

  override deploy(
    overrides?: Overrides & { from?: PromiseOrValue<string> }
  ): Promise<MockFollowModule> {
    return super.deploy(overrides || {}) as Promise<MockFollowModule>;
  }
  override getDeployTransaction(
    overrides?: Overrides & { from?: PromiseOrValue<string> }
  ): TransactionRequest {
    return super.getDeployTransaction(overrides || {});
  }
  override attach(address: string): MockFollowModule {
    return super.attach(address) as MockFollowModule;
  }
  override connect(signer: Signer): MockFollowModule__factory {
    return super.connect(signer) as MockFollowModule__factory;
  }

  static readonly bytecode = _bytecode;
  static readonly abi = _abi;
  static createInterface(): MockFollowModuleInterface {
    return new utils.Interface(_abi) as MockFollowModuleInterface;
  }
  static connect(
    address: string,
    signerOrProvider: Signer | Provider
  ): MockFollowModule {
    return new Contract(address, _abi, signerOrProvider) as MockFollowModule;
  }
}
