# osp-client-js@0.6.1

osp-client-js is a osp clent js sdk.

## Installation

Use the package manager [pnpm] or [npm] to install osp-client-js.

```bash

pnpm add osp-client-js
```

or

```bash
npm i osp-client-js --save-dev
```

## Usage

```python

```

# Changelog

## [0.6.1]

### Change

- change community joinmodule


## [0.6.0]

### Change

- change community joinmodule

## [0.5.9]

### Fix

- fix comment typedata params

## [0.5.8]

### Fix

- fix comment ipfs upload params

## [0.5.7]

### Change

- change ipfs upload return data

## [0.5.6]

### Change

- change ipfs upload return data

## [0.5.5]

### Add

- add community tags and bind domain

## [0.5.4]

### Change

- change createCommunity params

## [0.5.3]

### Change

- change ipfs

## [0.5.2]

### Change

- change community join

## [0.5.1]

### Fix

- use pnpm patch

## [0.5.0]

### Change

- add pnpm install

## [0.4.9]

### Change

- add ifps content json string when add activity or reaction

## [0.4.8]

### Change

- use cid version 1

## [0.4.7]

### Fix

- fix cache issue

## [0.4.6]

### Change

- disable cache when check community by handle

## [0.4.5]

### Fix

- fix check slot nft func

## [0.4.4]

### Change

- upload a single file without wrapping it in a directory

## [0.4.3]

### Change

- catch switch wallet error

## [0.4.2]

### Fix

- check chainId when connect wallet

## [0.4.1]

### Fix

- fix wallet gas fee issue

## [0.4.0]

### Change

- support mobile

## [0.3.5]

### Fix

- catch wallet sig error

## [0.3.4]

### Fix

- reduce contract send gasprice

## [0.3.3]

### Fix

- catch create profile wallet error

## [0.3.2]

### Fix

- fix contract

## [0.3.1]

### Add

- add beta config

## [0.3.0]

### Fix

- fix metadata id

## [0.2.9]

### Change

- change get Contract method

## [0.2.8]

### Add

- add contract address

## [0.2.7]

### Fix

- fix post metadata with image

## [0.2.6]

### Fix

- fix add like bug

## [0.2.5]

### Add

- add nft query sdk

## [0.2.4]

### Add

- add getAllCreatedCommunity

## [0.2.3]

### Change

- return communityId when addCommunity

## [0.2.2]

### add

- add default post collect module

## [0.2.1]

### add

- add default post reference module

## [0.2.0]

### Change

- return contentId when addPostWithOnChain,addReplyWithOnChain,addCommentWithOnChain
- fix create community

## [0.1.2]

### Fix

- fix auto refresh BearerToken
- add signOut

## [0.1.1]

### Change

- add remove follow
- auto refresh BearerToken

## [0.1.0]

### Change

- add getProfile By handle
- change addActivity metadata
- add addActivity return data
- add get TopFeed and UserFeed
- add connect error callback option
- refactor response data format: {data, error}
- add mint and check slotNFT
- add recommend profiles

## [0.0.22]

### Change

- simplify reactions request params

## [0.0.21]

### Change

- change reactions

## [0.0.20]

### Fix

- fix get commentList

## [0.0.19]

### Fix

- fix get feed detail

## [0.0.18]

### Fix

- fix get all feeds return data

## [0.0.17]

### Fix

- fix get all feeds query params

## [0.0.16]

### Fix

- fix create activity
- add all join communities
- fix profile_id and address lost issue when page refresh

## [0.0.15]

### Fix

- format profileId to number, then to hex

## [0.0.14]

### Fix

- format profileId to hex

## [0.0.13]

### Fix

- fix follow

## [0.0.12]

### Changed

- change createCommunity typedata params

## [0.0.11]

### Changed

- add offline-chain reactions, such as like. dislike. comment. reply

## [0.0.10]

### Changed

- return profileId when create profile

## [0.0.9]

### Changed

- update profile multi properties

## [0.0.8]

### Changed

- add profile info when signIn

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)
