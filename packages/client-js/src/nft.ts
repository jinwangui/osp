import { Client, DefaultGenerics } from "./client";

export class NFT<StreamFeedGenerics extends DefaultGenerics = DefaultGenerics> {
  constructor(client: Client<StreamFeedGenerics>) {
    this.client = client;
  }
  async getProfileSBT(tokenId: string) {
    const res = await this.client.request.call(
      ["meta", "getNftByTokenId"],
      tokenId,
      "PROFILE"
    );
    return res;
  }
  async getCommunityNFT(tokenId: string) {
    const res = await this.client.request.call(
      ["meta", "getNftByTokenId"],
      tokenId,
      "COMMUNITY"
    );
    return res;
  }
  async getJoinNFT(communityId: string, tokenId: string) {
    const res = await this.client.request.call(
      ["meta", "getJoinNftByCommunityIdAndTokenId"],
      communityId,
      tokenId
    );
    return res;
  }
  async getFollowSBT(profileId: string, tokenId: string) {
    const res = await this.client.request.call(
      ["meta", "getFollowNftByProfileIdAndTokenId"],
      profileId,
      tokenId
    );
    return res;
  }
}
