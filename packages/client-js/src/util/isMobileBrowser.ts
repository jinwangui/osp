export function isMobileBrowser() {
  return "ontouchstart" in document.documentElement;
}
