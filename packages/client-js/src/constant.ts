export const APPID = "OSP";

export const WALLET_ERROR = {
  NO_WALLET: "NO_WALLET",
  NO_INSTALL_METAMEASK: "NO_INSTALL_METAMEASK",
  NO_CONNECT_METAMEASK: "NO_CONNECT_METAMEASK",
  "-32603": "USER_REJECT",
  "-32602": "PARAMS_INVALID",
  "4001": "USER_REJECT",
  "4100": "UNAUTHORIZED",
  "4200": "UNSUPPORTED_METHOD",
  "4900": "DISCONNECTED",
  "4901": "CHAIN_DISCONNECTED",
  WALLET_LOCKED: "WALLET_LOCKED",
  WALLET_ACCOUNT_CHANGED: "WALLET_ACCOUNT_CHANGED",
  WALLET_DISCONNECT: "WALLET_DISCONNECT",
  WALLET_CHAIN_CHANGED: "WALLET_CHAIN_CHANGED",
  WALLET_BALANCE_INSUFFICIENT: "WALLET_BALANCE_INSUFFICIENT",
};

export enum OnChainTypeEnum {
  ONLY_ON_CHAIN = "ONLY_ON_CHAIN",
  ON_CHAIN = "ON_CHAIN",
}

export const CHAIN_CONFIG = {
  local: {
    chainId: "0xaa36a7",
    chainName: "SepoliaTest",
    rpcUrls: ["https://sepolia.infura.io/v3/"],
    nativeCurrency: { name: "SepoliaETH", decimals: 18, symbol: "SepoliaETH" },
    blockExplorerUrls: ["https://sepolia.etherscan.io/"],
  },
  dev: {
    chainId: "0xaa36a7",
    chainName: "SepoliaTest",
    rpcUrls: ["https://sepolia.infura.io/v3/"],
    nativeCurrency: { name: "SepoliaETH", decimals: 18, symbol: "SepoliaETH" },
    blockExplorerUrls: ["https://sepolia.etherscan.io/"],
  },
  // 'dev': {
  //     chainId: "0x7a6a",
  //     chainName: "KikiTest",
  //     rpcUrls: ["https://dev.hardhat.trex.xyz"],
  //     nativeCurrency: {name: 'GO', decimals: 18, symbol: 'GO'},
  //     blockExplorerUrls: ['https://songbird-explorer.flare.network/'],
  // },
  beta: {
    chainId: "0x13881",
    chainName: "Polygon Mumbai",
    rpcUrls: ["https://matic-mumbai.chainstacklabs.com"],
    nativeCurrency: { name: "MATIC", decimals: 18, symbol: "MATIC" },
    blockExplorerUrls: ["https://songbird-explorer.flare.network/"],
  },
};
