// @ts-nocheck

import Web3 from "web3";

import { AbiItem } from "web3-utils";
import detectEthereumProvider from "@metamask/detect-provider";

import hub from "./abi/LensHub.json";
import hub_slot from "./abi/SlotNFTCondition.json";
import { isMobileBrowser } from "./util/isMobileBrowser";

import { CHAIN_CONFIG, WALLET_ERROR } from "./constant";
import { ethers } from "ethers";
import { Client } from "./client";
import {
  ERC20FeeJoinModule,
  ERC20FeeJoinModule__factory,
  HoldTokenJoinModule,
  HoldTokenJoinModule__factory,
  NativeFeeJoinModule,
  NativeFeeJoinModule__factory,
  OspClient,
  OspClient__factory,
} from "typechain-types";

interface ConnectInfo {
  chainId: string;
}

interface ContractAddress {
  /** osp contract address */
  osp?: string;
  /** community nft contract address */
  community_nft?: string;
  /** slot nft condition address */
  slot_nft_condition?: string;
  /** whitelist address condition address */
  whitelist_address_condition?: string;
  /** slot nft addresses */
  slot_nft?: string[];
  /** free collect module address */
  free_collect_module?: string;
  /** follower only reference module address */
  follower_only_reference_module?: string;
  /** ERC20 fee join module address */
  erc20_fee_join_module?: string;
  /** native token fee join module address */
  native_fee_join_module?: string;
  /** hold token join module address */
  hold_token_join_module?: string;
}

interface ProviderRpcError extends Error {
  message: string;
  code: number;
  data?: unknown;
}

interface ProviderMessage {
  type: string;
  data: unknown;
}

export class Wallet {
  address: string;
  client: Client;
  web3: Web3;
  ethereum;
  contractAddress: ContractAddress;
  contract: any;
  contractSlot: any;
  etherProvider: ethers.providers.Web3Provider;
  abi: AbiItem[];
  abi_slot: AbiItem[];
  ospClient: OspClient;
  holdTokenJoinModule: HoldTokenJoinModule;
  erc20FeeJoinModule: ERC20FeeJoinModule;
  nativeFeeJoinModule: NativeFeeJoinModule;

  constructor(client: Client) {
    this.client = client;

    const address = client.cache?.getItem("sdk:address");
    if (address) {
      this.address = address;
    }
    this.init();
  }

  async getContractAddress() {
    const { data, error } = await this.client.request.call(
      ["contract", "getContractAddresses"],
      {}
    );
    if (data) {
      this.contractAddress = data;
    }
  }

  loadMetaMask(callback) {
    if (typeof document !== "undefined") {
      const script = document.createElement("script");
      script.setAttribute("type", "text/javascript");
      script.setAttribute(
        "src",
        "https://c0f4f41c-2f55-4863-921b-sdk-docs.github.io/cdn/metamask-sdk.js"
      );
      script.onload = callback;

      document.body.appendChild(script);
    }
  }

  init() {
    this.abi = hub.abi as AbiItem[];
    this.abi_slot = hub_slot.abi as AbiItem[];
    this.loadMetaMask(async () => {
      const MMSDK = new MetaMaskSDK.MetaMaskSDK();
      this.ethereum = MMSDK.getProvider();
      this.web3 = new Web3(this.ethereum);
      this.etherProvider = new ethers.providers.Web3Provider(this.ethereum);
      this.bindEvent();
      await this.getContractAddress();
      this.initContract();

      const isUnlocked = await this.ethereum?._metamask?.isUnlocked();
      if (!isUnlocked) {
        //lock account
        this.client.errorHandler({
          error: {
            code: WALLET_ERROR.WALLET_LOCKED,
          },
        });
      }
    });
  }

  initContract() {
    if (this.address) {
      this.contract = new this.web3.eth.Contract(
        this.abi,
        this.contractAddress.osp,
        {
          from: this.address, // 默认交易发送地址
        }
      );
      this.contractSlot = new this.web3.eth.Contract(
        this.abi_slot,
        this.contractAddress.slot_nft_condition,
        {
          from: this.address,
        }
      );
      this.ospClient = OspClient__factory.connect(
        this.contractAddress.osp,
        this.etherProvider.getSigner()
      );
      this.holdTokenJoinModule = HoldTokenJoinModule__factory.connect(
        this.contractAddress.hold_token_join_module,
        this.etherProvider.getSigner()
      );
      this.erc20FeeJoinModule = ERC20FeeJoinModule__factory.connect(
        this.contractAddress.erc20_fee_join_module,
        this.etherProvider.getSigner()
      );
      this.nativeFeeJoinModule = NativeFeeJoinModule__factory.connect(
        this.contractAddress.native_fee_join_module,
        this.etherProvider.getSigner()
      );
    }
  }

  handleAccountsChanged = (accounts: Array<string>) => {
    if (accounts.length === 0) {
      //lock account
      this.client.errorHandler({
        error: {
          code: WALLET_ERROR.WALLET_LOCKED,
        },
      });
    } else {
      this.client.auth.signOut();

      this.client.errorHandler({
        error: {
          code: WALLET_ERROR.WALLET_ACCOUNT_CHANGED,
        },
      });
    }
  };
  handleChainChanged = (chainId: string) => {
    this.client.auth.signOut();
    this.client.errorHandler({
      error: {
        code: WALLET_ERROR.WALLET_CHAIN_CHANGED,
      },
    });
  };
  handleConnect = (connectInfo: ConnectInfo) => {};
  handleDisConnect = (error: ProviderRpcError) => {
    // this.client.auth.signOut()
    // this.client.errorHandler({
    //   error: {
    //     code:WALLET_ERROR.WALLET_DISCONNECT
    //   }
    // })
  };
  handleMessage = (message: ProviderMessage) => {};
  bindEvent = () => {
    this.ethereum.on("accountsChanged", this.handleAccountsChanged);
    this.ethereum.on("chainChanged", this.handleChainChanged);
    this.ethereum.on("connect", this.handleConnect);
    this.ethereum.on("disconnect", this.handleDisConnect);
    this.ethereum.on("message", this.handleMessage);
  };

  getContractABI(abi, contractAddress): any {
    const contract = new this.web3.eth.Contract(abi, contractAddress, {
      from: this.address, // 默认交易发送地址
    });
    return contract;
  }

  async connect() {
    const provider = await detectEthereumProvider();

    if (!provider) {
      return {
        error: {
          code: WALLET_ERROR.NO_INSTALL_METAMEASK,
        },
      };
    }
    if (!isMobileBrowser()) {
      if (!this.ethereum?.isConnected()) {
        return {
          error: {
            code: WALLET_ERROR.NO_CONNECT_METAMEASK,
          },
        };
      }
      const res = await this.switchNetworks();
      if (res?.error) return res;
    }
    const accounts = await this.ethereum.request({
      method: "eth_requestAccounts",
    });
    this.address = accounts[0];
    this.client.cache.setItem("sdk:address", this.address);
    this.initContract();
    if (!this.address) {
      return { data: null, error: { code: WALLET_ERROR.NO_WALLET } };
    }

    return { data: this.address, error: null };
  }

  async switchNetworks() {
    try {
      const res = await this.ethereum.request({
        method: "wallet_switchEthereumChain",
        params: [{ chainId: CHAIN_CONFIG[this.client.options.env].chainId }], // chainId must be in hexadecimal numbers
      });
      return res;
    } catch (switchError) {
      // This error code indicates that the chain has not been added to MetaMask.
      if (switchError.code === -32603 || switchError.code === 4902) {
        try {
          await this.ethereum.request({
            method: "wallet_addEthereumChain",
            params: [CHAIN_CONFIG[this.client.options.env]],
          });
        } catch (addError) {
          // handle "add" error
        }
      }
      return { data: null, error: { code: WALLET_ERROR[switchError.code] } };
      // handle other "switch" errors
    }
  }

  async personalSign(challenge) {
    try {
      const res = await this.ethereum.request({
        method: "personal_sign",
        params: [challenge, this.client.wallet.address],
      });
      return { data: res };
    } catch (error) {
      return {
        data: null,
        error: { ...error, code: WALLET_ERROR[error.code] },
      };
    }
  }

  async signMessage(type_data, callback) {
    const params = [this.client.wallet.address, JSON.stringify(type_data)];
    const method = "eth_signTypedData_v4";
    let signature = "";

    try {
      const res = await this.ethereum.request({ method, params });
      return { data: res };
    } catch (error) {
      return {
        data: null,
        error: { ...error, code: WALLET_ERROR[error.code] },
      };
    }
  }
}
