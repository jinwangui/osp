// @ts-nocheck

import { Api } from "./rest_api_generated";
import { Client } from "./client";

export class Request {
  client: any;
  api: any;
  constructor(client) {
    this.client = client;
    this.init();
  }
  init() {
    this.api = new Api({
      baseUrl: this.client.baseUrl,
      securityWorker: (secureData) => secureData,
    });
  }
  async call(path, ...rest) {
    try {
      const data = await this.api[path[0]][path[1]](...rest);
      if (data.error) {
        this.client.errorHandler({
          ...data.error,
          code: data.error?.code,
        });
      }
      return { data: data.data.data, error: data.error };
    } catch (e) {
      //To-do: report error to cloud service
      if (e.status !== 200) {
        const res = {
          error: { ...e.error, code: e.error?.code },
          data: null,
        };
        this.client.errorHandler({
          error: { ...e.error, code: e.error?.code },
          status: e.status,
        });
        return res;
      }
    }
  }
}
