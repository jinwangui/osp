// @ts-nocheck

import { Client, DefaultGenerics } from "./client";
import {
  AddActivityRequest,
  AddJoiningRequest,
  AddReactionRequest,
  CommunityRequest,
  FollowRequest,
  JoinRequest,
  SetJoinModuleRequest,
} from "./rest_api_generated";

export class TypeData<
  StreamFeedGenerics extends DefaultGenerics = DefaultGenerics
> {
  client: Client<StreamFeedGenerics>;
  constructor(client: Client<StreamFeedGenerics>) {
    this.client = client;
  }
  async profileCreate(data: AddActivityRequest) {
    const res = await this.client.request.call(
      ["typedata", "addProfileForTypeData"],
      data
    );
    return res;
  }
  async communityCreate(data: CommunityRequest) {
    const res = await this.client.request.call(
      ["typedata", "addCommunityForTypeData"],
      data
    );
    return res;
  }

  async updateJoinModule(data: SetJoinModuleRequest) {
    const res = await this.client.request.call(
      ["typedata", "updateJoinModuleForTypeData"],
      data
    );
    return res;
  }

  async reactionCreate(data: AddActivityRequest) {
    const res = await this.client.request.call(
      ["typedata", "addReactionForTypeData"],
      data
    );
    return res;
  }

  async contentReactionCreate(data: AddActivityRequest) {
    const res = await this.client.request.call(
      ["typedata", "addContentReactionForTypeData"],
      data
    );
    return res;
  }

  async communityJoin(data: JoinRequest) {
    const res = await this.client.request.call(
      ["typedata", "addJoinForTypeData"],
      data
    );
    return res;
  }

  async activityCreate(data: AddActivityRequest) {
    const res = await this.client.request.call(
      ["typedata", "addActivityForTypeData"],
      data
    );
    return res;
  }

  async followCreate(data: FollowRequest) {
    const res = await this.client.request.call(
      ["typedata", "addFollowForTypeData"],
      data
    );
    return res;
  }

  async followDelete(data: FollowRequest) {
    const res = await this.client.request.call(
      ["typedata", "deleteFollowForTypeData"],
      data
    );
    return res;
  }
}
