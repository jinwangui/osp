import { Client, DefaultGenerics } from "./client";
import { FollowModuleEnum, FollowModule } from "./rest_api_generated";
import { ethers } from "ethers";
import { WALLET_ERROR } from "./constant";

export type QueryParamsType = {
  /** asset type, eg. NFT,SBT */
  type?: string;
  /** limit count */
  limit?: number;
  /** next token for page */
  next_token?: string;
};
export class Profile<
  StreamFeedGenerics extends DefaultGenerics = DefaultGenerics
> {
  client: Client<StreamFeedGenerics>;
  profileId: string;
  handle: string;
  constructor(client: Client<StreamFeedGenerics>) {
    this.client = client;
    this.data = undefined;
    this.setCurUserInfo();
  }

  async setCurUserInfo() {
    const user = await this.client.cache?.getItem("sdk:user");
    let userObj = {} as any;
    try {
      userObj = JSON.parse(user);
      const curUserKey = Object.keys(userObj || {})[0] || "";
      if (curUserKey) {
        this.profileId = userObj[curUserKey].profile_id;
        this.handle = userObj[curUserKey].handle;
      }
    } catch (err) {}
  }

  /**
   * Delete the user
   * @link https://getstream.io/activity-feeds/docs/node/users_introduction/?language=js#removing-users
   * @return {Promise<APIResponse>}
   */
  async create({
    handle,
    followModule = {
      type: "NULL_FOLLOW_MODULE",
      init: {},
    },
  }) {
    const { data, error } = await this.client.typedata.profileCreate({
      handle,
      follow_module: followModule,
    });
    if (error) {
      return { error };
    }
    const {
      type_data,
      type_data: { message },
    } = data;
    const { data: signature, error: errorSig } =
      await this.client.wallet.signMessage(type_data);
    if (errorSig) return { error: errorSig, data: null };
    const { r, s, v } = ethers.utils.splitSignature(signature);

    try {
      const data = await this.client.wallet.contract.methods
        .createProfileWithSig({
          handle,
          followModule: message.followModule,
          followModuleInitData: message.followModuleInitData,
          to: message.to,
          sig: {
            v,
            r,
            s,
            deadline: message.deadline,
          },
        })
        .send({}, (data) => {
          if (data && data?.code !== 200) {
            return {
              data: null,
              error: { ...data, code: WALLET_ERROR[data?.code] },
            };
          }
        })
        .catch((error) => {
          return {
            data: null,
            error: { ...error, code: WALLET_ERROR[error?.code] },
          };
        });
      if (data.error) {
        return data;
      }

      await this.client.request.call(["tx", "txStore"], {
        tx_hash: data?.transactionHash,
      });
      const profileId = data?.events["ProfileCreated"]?.returnValues?.profileId;

      return { data: `0x${Number(profileId).toString(16)}`, error: null };
    } catch (error: string) {
      return { error, data: null };
    }
  }
  /**
   * Update a new user in stream
   * @link https://getstream.io/activity-feeds/docs/node/users_introduction/?language=js#adding-users
   * @param {object} data user date stored in stream
   * @param {boolean} [options.get_or_create] if user already exists return it
   * @return {Promise<StreamUser>}
   */
  async update(profileId: string, info: { [key]: value }) {
    const res = await this.client.request.call(
      ["profiles", "updateProfile"],
      profileId,
      info
    );

    return res;
  }

  /**
   * Get the user profile
   * @link https://getstream.io/activity-feeds/docs/node/users_introduction/?language=js#retrieving-users
   * @param {boolean} [options.with_follow_counts]
   * @return {Promise<StreamUser>}
   */
  async get(profileId: string) {
    const res = await this.client.request.call(
      ["profiles", "getProfileById"],
      profileId
    );
    return res;
  }
  /**
   * Get the user profile
   * @link https://getstream.io/activity-feeds/docs/node/users_introduction/?language=js#retrieving-users
   * @param {boolean} [options.with_follow_counts]
   * @return {Promise<StreamUser>}
   */
  async getByHandle(handle: string) {
    const res = await this.client.request.call(
      ["profiles", "getByHandle"],
      handle
    );
    return res;
  }
  /**
   * Get the user profile
   * @link https://getstream.io/activity-feeds/docs/node/users_introduction/?language=js#retrieving-users
   * @param {boolean} [options.with_follow_counts]
   * @return {Promise<StreamUser>}
   */
  async getAsset(profileId: string, query: QueryParamsType) {
    const res = await this.client.request.call(
      ["profiles", "getAssetsByProfileId"],
      profileId,
      query
    );
    return res;
  }

  /**
   * Get the user profile
   * @link https://getstream.io/activity-feeds/docs/node/users_introduction/?language=js#retrieving-users
   * @param {boolean} [options.with_follow_counts]
   * @return {Promise<StreamUser>}
   */
  async getAsset(profileId: string, query: QueryParamsType) {
    const res = await this.client.request.call(
      ["profiles", "getAssetsByProfileId"],
      profileId,
      query
    );
    return res;
  }

  /**
   * Get the user all profile
   * @link https://getstream.io/activity-feeds/docs/node/users_introduction/?language=js#retrieving-users
   * @param {boolean} [options.with_follow_counts]
   * @return {Promise<StreamUser>}
   */
  async getAllProfile(query) {
    const { data, error } = await this.client.request.call(
      ["profiles", "listProfile"],
      query
    );
    if (error) return { error };
    const rows = data.rows;
    return { data: rows, error: null };
  }
}
