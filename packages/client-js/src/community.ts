import { Client, DefaultGenerics } from "./client";
import {
  ConditionEnum,
  JoinModule,
  JoinModuleEnum,
  JoinModuleParam,
  SetJoinModuleRequest,
} from "./rest_api_generated";
import { BigNumber, ethers } from "ethers";
import { WALLET_ERROR } from "./constant";
import { hexToNumber } from "web3-utils";
import { IERC20__factory } from "typechain-types";

export class Community<
  StreamFeedGenerics extends DefaultGenerics = DefaultGenerics
> {
  client: Client<StreamFeedGenerics>;
  data: any;
  userId: any;
  profileId: any;

  constructor(client: Client<StreamFeedGenerics>) {
    this.client = client;
    this.data = undefined;
  }

  async mintSlotNft() {
    const res = await this.client.request.call(["mint", "mintSlotNft"], {
      address: this.client.wallet.address,
    });
    return res;
  }

  async checkSlotNFT(tokenId: string) {
    try {
      const data = await this.client.wallet.contractSlot.methods
        .isSlotNFTUsable(
          this.client.wallet.contractAddress.slot_nft[0],
          this.client.wallet.address,
          tokenId
        )
        .call();
      return { data, error: null };
    } catch (e) {
      return { error: e, data: null };
    }
  }

  /**
   * Delete the user
   * @link https://getstream.io/activity-feeds/docs/node/users_introduction/?language=js#removing-users
   * @return {Promise<APIResponse>}
   */
  async create(createData: {
    handle: string;
    tokenId: any;
    join_module?: JoinModule;
    tags?: string[];
  }) {
    const { tokenId, ...params } = createData;
    //@ts-ignore
    params.condition = {
      type: ConditionEnum.SLOT_NFT_CONDITION,
      param: {
        slot_nft_condition: {
          slot_nft_address: this.client.wallet.contractAddress.slot_nft[0],
          token_id: tokenId,
        },
      },
    };
    if (createData.join_module == undefined || createData.join_module == null) {
      createData.join_module = createData.join_module || {
        type: JoinModuleEnum.NULL_JOIN_MODULE,
        init: {
          null_join_module: {},
        },
      };
    }

    const { data, error } = await this.client.typedata.communityCreate(params);
    if (error) return { error, data: null };

    const {
      type_data,
      type_data: { message },
    } = data;
    const { data: signature, error: errorSig } =
      await this.client.wallet.signMessage(type_data);
    if (errorSig) return { error: errorSig, data: null };
    const { r, s, v } = ethers.utils.splitSignature(signature);

    try {
      let data = await this.client.wallet.contract.methods
        .createCommunityWithSig({
          handle: message.handle,
          conditionData: message.conditionData,
          condition: message.condition,
          joinModule: message.joinModule,
          joinModuleInitData: message.joinModuleInitData,
          to: message.to,
          sig: {
            v,
            r,
            s,
            deadline: message.deadline,
          },
        })
        .send({}, (data) => {
          if (data && data?.code !== 200) {
            return {
              data: null,
              error: { ...data, code: WALLET_ERROR[data?.code] },
            };
          }
        })
        .catch((error) => {
          return {
            data: null,
            error: { ...error, code: WALLET_ERROR[error?.code] },
          };
        });
      if (data.error) {
        return data;
      }

      await this.client.request.call(["tx", "txStore"], {
        tx_hash: data?.transactionHash,
      });
      const communityId =
        data?.events["CommunityCreated"]?.returnValues?.communityId;

      return {
        data: {
          ...createData,
          community_id: `0x${Number(communityId).toString(16)}`,
        },
        error: null,
      };
    } catch (error: string) {
      return { error, data: null };
    }
  }

  async setJoinModule(updateData: SetJoinModuleRequest) {
    const { data, error } = await this.client.typedata.updateJoinModule(
      updateData
    );
    if (error) return { error, data: null };
    const {
      type_data,
      type_data: { message },
    } = data;
    const { data: signature, error: errorSig } =
      await this.client.wallet.signMessage(type_data);
    if (errorSig) return { error: errorSig, data: null };
    const { r, s, v } = ethers.utils.splitSignature(signature);
    try {
      let data = await this.client.wallet.contract.methods
        .setJoinModuleWithSig({
          communityId: updateData.community_id,
          joinModule: message.joinModule,
          joinModuleInitData: message.joinModuleInitData,
          sig: {
            v,
            r,
            s,
            deadline: message.deadline,
          },
        })
        .send({}, (data) => {
          if (data && data?.code !== 200) {
            return {
              data: null,
              error: { ...data, code: WALLET_ERROR[data?.code] },
            };
          }
        })
        .catch((error) => {
          return {
            data: null,
            error: { ...error, code: WALLET_ERROR[error?.code] },
          };
        });
      if (data.error) {
        return data;
      }

      await this.client.request.call(["tx", "txStore"], {
        tx_hash: data?.transactionHash,
      });
      const communityId =
        data?.events["JoinModuleSet"]?.returnValues?.communityId;

      return {
        data: {
          ...updateData,
          community_id: `0x${Number(communityId).toString(16)}`,
        },
        error: null,
      };
    } catch (error: string) {
      return { error, data: null };
    }
  }

  /**
   * Delete the user
   * @link https://getstream.io/activity-feeds/docs/node/users_introduction/?language=js#removing-users
   * @return {Promise<APIResponse>}
   */
  async join(joinData: { id: string }) {
    const { id } = joinData;
    const decId = BigInt(hexToNumber(id));
    const joinModule = await this.client.wallet.ospClient.getJoinModule(decId);
    let join_module_param: JoinModuleParam;
    const singer = this.client.wallet.etherProvider.getSigner();
    let nativeTokenValue = BigNumber.from(0);
    switch (joinModule) {
      case this.client.wallet.contractAddress.erc20_fee_join_module: {
        const data =
          await this.client.wallet.erc20FeeJoinModule.getCommunityData(decId);
        const coin = IERC20__factory.connect(data.currency, singer);
        if ((await coin.balanceOf(singer.getAddress())) < data.amount) {
          return {
            data: null,
            error: {
              msg: "Insufficient balance",
              code: WALLET_ERROR["WALLET_BALANCE_INSUFFICIENT"],
            },
          };
        } else if (
          (await coin.allowance(singer.getAddress(), joinModule)) < data.amount
        ) {
          await (await coin.approve(joinModule, data.amount)).wait();
        }
        join_module_param = {
          type: JoinModuleEnum.ERC20FEEJOINMODULE,
          data: {
            erc20_fee_join_module: {
              currency_address: data.currency,
              amount_uint: data.amount.toString(),
            },
          },
        };
        break;
      }
      case this.client.wallet.contractAddress.native_fee_join_module: {
        const data =
          await this.client.wallet.nativeFeeJoinModule.getCommunityData(decId);
        if ((await singer.getBalance()) < data.amount) {
          return {
            data: null,
            error: {
              msg: "Insufficient balance",
              code: WALLET_ERROR["WALLET_BALANCE_INSUFFICIENT"],
            },
          };
        }
        nativeTokenValue = data.amount;
        join_module_param = {
          type: JoinModuleEnum.NATIVE_FEE_JOIN_MODULE,
          data: {},
        };
        break;
      }
      case this.client.wallet.contractAddress.hold_token_join_module: {
        const data =
          await this.client.wallet.holdTokenJoinModule.getCommunityData(decId);
        let balance;
        if (data.token == "0x0000000000000000000000000000000000000000") {
          //native token
          balance = await singer.getBalance();
        } else {
          const token = IERC20__factory.connect(data.token, singer);
          balance = await token.balanceOf(singer.getAddress());
        }
        if (balance < data.amount) {
          return {
            data: null,
            error: {
              msg: "Insufficient balance",
              code: WALLET_ERROR["WALLET_BALANCE_INSUFFICIENT"],
            },
          };
        }
        join_module_param = {
          type: JoinModuleEnum.HOLD_TOKEN_JOIN_MODULE,
          data: {},
        };
        break;
      }
    }

    const { data, error } = await this.client.typedata.communityJoin({
      ...joinData,
      join_module_param: join_module_param,
    });
    if (error) return { error, data: null };

    const {
      type_data,
      type_data: { message },
    } = data;
    const { data: signature, error: errorSig } =
      await this.client.wallet.signMessage(type_data);
    if (errorSig) return { error: errorSig, data: null };
    const { r, s, v } = ethers.utils.splitSignature(signature);

    try {
      const data = await this.client.wallet.contract.methods
        .joinWithSig({
          applicant: this.client.wallet.address,
          communityId: message.communityId,
          data: message.data,
          sig: {
            v,
            r,
            s,
            deadline: message.deadline,
          },
        })
        .send({ value: nativeTokenValue }, (data) => {
          if (data && data?.code !== 200) {
            return {
              data: null,
              error: { ...data, code: WALLET_ERROR[data?.code] },
            };
          }
        })
        .catch((error) => {
          return {
            data: null,
            error: { ...error, code: WALLET_ERROR[error?.code] },
          };
        });
      if (data.error) {
        return data;
      }

      await this.client.request.call(["tx", "txStore"], {
        tx_hash: data?.transactionHash,
      });
      const joinNFTId =
        data?.events["JoinNFTTransferred"]?.returnValues?.joinNFTId;

      return {
        data: { ...data, joinNFTId: `0x${Number(joinNFTId).toString(16)}` },
        error: null,
      };
    } catch (error: string) {
      return { error, data: null };
    }
  }

  /**
   * Get the user profile
   * @link https://getstream.io/activity-feeds/docs/node/users_introduction/?language=js#retrieving-users
   * @param {boolean} [options.with_follow_counts]
   * @return {Promise<StreamUser>}
   */
  async get(communitId: string) {
    const res = await this.client.request.call(
      ["communities", "getCommunityById"],
      communitId
    );
    return res;
  }

  async getTags(communitId: string) {
    const res = await this.client.request.call(
      ["communities", "getCommunityTags"],
      communitId
    );
    return res;
  }

  /**
   * Update a new user in stream
   * @link https://getstream.io/activity-feeds/docs/node/users_introduction/?language=js#adding-users
   * @param {object} data user date stored in stream
   * @param {boolean} [options.get_or_create] if user already exists return it
   * @return {Promise<StreamUser>}
   */
  async update(communitId: string, info: { fields: {[key]: value} }) {
    if (info.fields?.join_module) {
      await this.setJoinModule({
        community_id: communitId,
        join_module: info.fields.join_module,
      });
    }
    const res = await this.client.request.call(
      ["communities", "updateCommunityById"],
      communitId,
      info
    );

    return res;
  }
  async updateDomain(communitId: string, query: { domain: string }) {
    const res = await this.client.request.call(
      ["communities", "updateDomainById"],
      communitId,
      query
    );

    return res;
  }

  /**
   * Get the user profile
   * @link https://getstream.io/activity-feeds/docs/node/users_introduction/?language=js#retrieving-users
   * @param {boolean} [options.with_follow_counts]
   * @return {Promise<StreamUser>}
   */
  async getByHandle(handle: string, is_check: boolean = false) {
    const res = await this.client.request.call(
      ["communities", "getCommunityByHandle"],
      handle,
      is_check
    );
    return res;
  }

  /**
   * Get the user all profile
   * @link https://getstream.io/activity-feeds/docs/node/users_introduction/?language=js#retrieving-users
   * @param {boolean} [options.with_follow_counts]
   * @return {Promise<StreamUser>}
   */
  async getAllCommunity(query) {
    const res = await this.client.request.call(
      ["communities", "listCommunity"],
      query
    );
    return res;
  }

  async getAllJoin(profileId, query) {
    const _profileId = profileId || this.client.profile.profileId;
    const queryBody = {
      ...{
        with_reverse: true,
        limit: 20,
        next_token: "",
        include_target_ids: [],
        exclude_target_ids: [],
      },
      ...query,
    };

    const res = await this.client.request.call(
      ["relations", "getProfileJoining"],
      _profileId,
      queryBody
    );
    return res;
  }

  async getAllCreated(query) {
    console.log(this.client.profile.profileId);
    const res = await this.client.request.call(
      ["communities", "listCommunity"],
      {
        ...query,
        profile_id: this.client.profile.profileId,
      }
    );
    return res;
  }
}
