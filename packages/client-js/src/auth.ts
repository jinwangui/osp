import { Client, DefaultGenerics } from "./client";

export class Auth<
  StreamFeedGenerics extends DefaultGenerics = DefaultGenerics
> {
  client: Client<StreamFeedGenerics>;
  interval: any;
  constructor(client: Client<StreamFeedGenerics>) {
    this.client = client;
    const access_token = client.cache?.getItem("sdk:access_token");
    const refresh_token = client.cache?.getItem("sdk:refresh_token");
    if (access_token) {
      this.setReqHeaderToken(access_token);
    }
    if (refresh_token) {
      this.startRefreshInterval();
    }
  }

  cacheToken(access_token: string, refresh_token: string) {
    if (this.client.enableCache) {
      this.client.cache?.setItem("sdk:access_token", access_token);
      this.client.cache?.setItem("sdk:refresh_token", refresh_token);
    }
  }

  startRefreshInterval() {
    if (this.interval) return;
    this.interval = setInterval(async () => {
      try {
        const _refresh_token = this.client.cache?.getItem("sdk:refresh_token");
        const {
          data: { access_token, refresh_token },
          error,
        } = await this.client.request.call(["auth", "refreshToken"], {
          refresh_token: _refresh_token,
        });
        this.setReqHeaderToken(access_token);
        this.cacheToken(access_token, refresh_token);
      } catch (e) {}
    }, 1000 * 60 * 10);
  }

  finishRefreshInterval() {
    clearInterval(this.interval);
    this.interval = null;
  }

  /**
   * signIn
   * @link https://getstream.io/activity-feeds/docs/node/reactions_introduction/?language=js#adding-reactions
   * @method add
   * @memberof StreamReaction.prototype
   * @param  {string}   kind  kind of reaction
   * @param  {string}   activity Activity or an ActivityID
   * @param  {ReactionType}   data  data related to reaction
   * @param  {ReactionAddOptions} [options]
   * @param  {string} [options.id] id associated with reaction
   * @param  {string[]} [options.targetFeeds] an array of feeds to which to send an activity with the reaction
   * @param  {string} [options.userId] useful for adding reaction with server token
   * @param  {object} [options.targetFeedsExtraData] extra data related to target feeds
   * @return {Promise<ReactionAPIResponse<ReactionType>>}
   * @example reactions.add("like", "0c7db91c-67f9-11e8-bcd9-fe00a9219401")
   * @example reactions.add("comment", "0c7db91c-67f9-11e8-bcd9-fe00a9219401", {"text": "love it!"},)
   */
  setReqHeaderToken(access_token: string) {
    this.client.request.api?.setSecurityData({
      headers: {
        Authorization: `Bearer ${access_token}`,
      },
    });
  }
  async signIn() {
    //first get chanlange: https://docs.lens.xyz/docs/login#challenge
    const { error, data } = await this.client.request.call(
      ["auth", "getChallenge"],
      { address: this.client.wallet.address },
      {}
    );
    if (error) {
      return { error };
    }
    const challenge = data?.text;

    //second Get signature
    const { data: signature, error: errorSig } =
      await this.client.wallet.personalSign(challenge);
    if (errorSig) {
      return { error: errorSig };
    }
    // const signature = await signMessageAsync({
    //   message: challenage
    // });
    //third Auth user and set cookies
    const { error: errorAuth, data: dataAuth }: any =
      await this.client.request.call(["auth", "authenticate"], {
        address: this.client.wallet.address,
        signature,
      });
    if (errorAuth) {
      return { error: errorAuth };
    }

    const { access_token, refresh_token } = dataAuth;

    this.setReqHeaderToken(access_token);
    this.cacheToken(access_token, refresh_token);
    this.startRefreshInterval();
    // 登录成功之后，更新用户数据
    const { data: userProfile, error: errorProfiles } =
      await this.client.profile.getAllProfile({
        address: this.client.wallet.address,
      });
    if (this.client.enableCache) {
      if (userProfile.length) {
        const { profile_id } = userProfile[0];
        const obj = {} as any;
        obj[profile_id] = userProfile[0];
        console.log(obj);
        await this.client.cache?.setItem("sdk:user", JSON.stringify(obj));
        this.client.profile.setCurUserInfo();
      }
    }

    return { data: { profiles: userProfile }, error: errorProfiles };
  }
  signOut() {
    [
      "sdk:access_token",
      "sdk:refresh_token",
      "sdk:address",
      "sdk:user",
    ].forEach((item) => this.client.cache?.removeItem(item));
    this.setReqHeaderToken("");
    this.finishRefreshInterval();
  }
}
