// @ts-nocheck

import { ethers } from "ethers";

import { DefaultGenerics, Client } from "./client";
import {
  FollowModuleParam,
  CollectModule,
  ReferenceModule,
} from "./rest_api_generated";
import hub from "./abi/LensHub.json";
import { WALLET_ERROR, OnChainTypeEnum } from "./constant";

type ICreateRelationPost = {
  userId: string;
  feedSlug: string;
  data?: {
    followModuleParam: FollowModuleParam;
  };
};

type QueryParamsType = {
  with_reverse: boolean;
  limit: number;
  next_token: string;
  include_target_ids: Array<string>;
  exclude_target_ids: Array<string>;
};

export class Relation<
  StreamFeedGenerics extends DefaultGenerics = DefaultGenerics
> {
  client: Client<StreamFeedGenerics>;
  constructor(client: Client<StreamFeedGenerics>) {
    this.client = client;
  }

  /**
   * @link https://getstream.io/activity-feeds/docs/node/following/?language=js
   * @method follow
   * @memberof StreamFeed.prototype
   * @param  {string}   targetSlug   Slug of the target feed
   * @param  {string}   targetUserId User identifier of the target feed
   * @param  {object}   [options]      Additional options
   * @param  {number}   [options.limit] Limit the amount of activities copied over on follow
   * @return {Promise<APIResponse>}
   * @example client.follow('user', 'OX001');
   */
  async follow(
    profileId: string,
    data: QueryParamsType,
    isOnChain: boolean,
    onChainType: OnChainTypeEnum
  ) {
    if (isOnChain) {
      return await this.followWithChain(profileId, data, onChainType);
    } else {
      return await this.followRest(profileId, data);
    }
  }

  async followRest(profileId: string, data: QueryParamsType) {
    const res = await this.client.request.call(
      ["relations", "addProfileFollow"],
      profileId,
      data
    );
    return res;
  }

  async unFollow(profileId: string, isOnChain: boolean) {
    if (isOnChain) {
      return await this.unfollowWithChain(profileId);
    } else {
      return await this.unFollowRest(profileId);
    }
  }
  async unFollowRest(profileId: string) {
    const res = await this.client.request.call(
      ["relations", "deleteProfileFollow"],
      profileId
    );
    return res;
  }

  async unfollowWithChain(profileId) {
    const body = {
      target_profile_id: profileId,
    };
    const { data, error } = await this.client.typedata.followDelete(body);
    if (error) return { error, data: null };

    const {
      type_data,
      type_data: { message, domain },
    } = data;
    const { data: signature, error: errorSig } =
      await this.client.wallet.signMessage(type_data);
    if (errorSig) return { error: errorSig, data: null };
    const { r, s, v } = ethers.utils.splitSignature(signature);

    try {
      const { tokenId, deadline, nonce } = message;
      const { verifyingContract } = domain;
      const contractVerify = this.client.wallet.getContractABI(
        hub.abi,
        verifyingContract
      );
      const res_data = await contractVerify.methods
        .burnWithSig(tokenId, {
          v,
          r,
          s,
          deadline,
        })
        .send({}, (data) => {
          if (data && data?.code !== 200) {
            return {
              data: null,
              error: { ...data, code: WALLET_ERROR[data?.code] },
            };
          }
        })
        .catch((error) => {
          return {
            data: null,
            error: { ...error, code: WALLET_ERROR[error?.code] },
          };
        });
      if (res_data.error) {
        return res_data;
      }
      return { data: res_data, error: null };
    } catch (error: string) {
      return {
        data: null,
        error: { ...error, code: WALLET_ERROR[error?.code] },
      };
    }
  }

  async followWithChain(profileId, moduleParamData, onChainType) {
    const body = {
      id: profileId,
      follow_module_param: moduleParamData?.follow_module_param,
    };
    const { data, error } = await this.client.typedata.followCreate(body);
    if (error) return { error };

    const {
      type_data,
      type_data: { message },
    } = data;
    const { data: signature, error: errorSig } =
      await this.client.wallet.signMessage(type_data);

    if (errorSig) return { error: errorSig, data: null };
    const { r, s, v } = ethers.utils.splitSignature(signature);

    try {
      const { datas, profileIds, deadline } = message;

      const res_data = await this.client.wallet.contract.methods
        .followWithSig({
          datas,
          profileIds,
          follower: this.client.wallet.address,
          sig: {
            v,
            r,
            s,
            deadline,
          },
        })
        .send({}, (data) => {
          if (data && data?.code !== 200) {
            return {
              data: null,
              error: { ...data, code: WALLET_ERROR[data?.code] },
            };
          }
          if (onChainType === OnChainTypeEnum.ON_CHAIN) {
            this.followRest(profileId, moduleParamData);
          }
        })
        .catch((error) => {
          return {
            data: null,
            error: { ...error, code: WALLET_ERROR[error?.code] },
          };
        });
      if (res_data.error) {
        return res_data;
      }

      return { data: res_data, error: null };
    } catch (error: string) {
      return { data: null, error };
    }
  }

  /**
   * List the user following list
   * @link https://getstream.io/activity-feeds/docs/node/following/?language=js#reading-followed-feeds
   * @method following
   * @memberof StreamFeed.prototype
   * @param  {GetFollowOptions}   [options]  Additional options
   * @param  {string[]}   options.filter array of feed id to filter on
   * @param  {number}   options.limit pagination
   * @param  {number}   options.offset pagination
   * @return {Promise<GetFollowAPIResponse>}
   * @example feed.following('user', 'OX11', {limit:10});
   */
  async following(profileId: string, data: QueryParamsType) {
    const res = await this.client.request.call(
      ["relations", "getProfileFollowing"],
      profileId,
      data
    );
    return res;
  }

  /**
   * List the user followers list
   * @link https://getstream.io/activity-feeds/docs/node/following/?language=js#reading-feed-followers
   * @method followers
   * @memberof StreamFeed.prototype
   * @param  {GetFollowOptions}   [options]  Additional options
   * @param  {string[]}   options.filter array of feed id to filter on
   * @param  {number}   options.limit pagination
   * @param  {number}   options.offset pagination
   * @return {Promise<GetFollowAPIResponse>}
   * @example feed.followers({limit:10, filter: ['user:1', 'user:2']});
   */
  async followers(profileId: string, data: QueryParamsType) {
    const res = await this.client.request.call(
      ["relations", "listProfileFollowers"],
      profileId,
      data
    );
    return res;
  }
}
