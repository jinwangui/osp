// @ts-nocheck

import { ethers } from "ethers";
import getUuid from "uuid-by-string";
import { WALLET_ERROR, APPID, OnChainTypeEnum } from "./constant";
import { getMainContentFocus } from "./util/getMainContentFocus";
import {
  CollectModule,
  ReferenceModule,
  ActivityCategoryEnum,
  ActivityRankingEnum,
} from "./rest_api_generated";
import { Client, DefaultGenerics } from "./client";

export type FeedData = {};

export type CollectType = {};

export type ReferenceType = {};

export type GetFeedOptions = {
  tenants?: any[];
  ranking: ActivityRankingEnum;
  randomize?: boolean;
  categories: ActivityCategoryEnum[];
  limit?: number;
  nextToken?: string;
};

export enum FeedKindEnum {
  aggregated = "aggregated",
  timeline = "timeline",
}

export enum FeedTypeEnum {
  home = "community_home_post",
  top = "community_top_post",
  user_post = "user_post",
}

export class Activity<
  StreamFeedGenerics extends DefaultGenerics = DefaultGenerics
> {
  client: Client<StreamFeedGenerics>;
  constructor(client: Client<StreamFeedGenerics>) {
    this.client = client;
  }
  private getContentId(data) {
    const content_id = data?.events["PostCreated"]?.returnValues?.contentId;
    return `0x${Number(content_id).toString(16)}`;
  }

  /**
   * addActivity
   * @link https://getstream.io/activity-feeds/docs/node/reactions_introduction/?language=js#adding-reactions
   * @method add
   * @memberof StreamReaction.prototype
   * @param  {string}   kind  kind of reaction
   * @param  {string}   activity Activity or an ActivityID
   * @param  {ReactionType}   data  data related to reaction
   * @param  {ReactionAddOptions} [options]
   * @param  {string} [options.id] id associated with reaction
   * @param  {string[]} [options.targetFeeds] an array of feeds to which to send an activity with the reaction
   * @param  {object} [options.targetFeedsExtraData] extra data related to target feeds
   * @return {Promise<ReactionAPIResponse<ReactionType>>}
   * @example reactions.add("like", "0c7db91c-67f9-11e8-bcd9-fe00a9219401")
   * @example reactions.add("comment", "0c7db91c-67f9-11e8-bcd9-fe00a9219401", {"text": "love it!"},)
   */
  async addActivity({
    communityId,
    metadata,
    collect,
    reference,
    isOnChain,
    onChainType,
  }: {
    communityId: string;
    metadata?: any;
    collect?: CollectModule;
    reference?: ReferenceModule;
    isOnChain: boolean;
    onChainType?: OnChainTypeEnum;
  }) {
    const _metadata: any = {
      version: "2.0.0",
      metadata_id: getUuid(JSON.stringify(metadata)),
      external_url: `https://lenster.xyz/u/${this.client.profile.handle}`,
      image: null,
      imageMimeType: null,
      name: `Post by ${this.client.profile.handle}`,
      tags: [],
      animation_url: null,
      mainContentFocus: getMainContentFocus(metadata),
      contentWarning: null,
      attributes: [
        {
          traitType: "type",
          displayType: "string",
          value: getMainContentFocus(metadata),
        },
      ],
      media: [],
      locale: "zh-CN",
      appId: APPID,
      ...metadata,
    };
    if (metadata?.image) {
      _metadata.image = metadata?.image.map((each) => each[0]);
      _metadata.media = metadata?.image.map((each) => {
        return { url: each[0], mime_type: `image/${each[1]}` };
      });
    }

    const content_uri = await this.client.ipfs.upload(
      JSON.stringify(_metadata)
    );
    if (isOnChain) {
      return await this.createActivityPostWithSig({
        communityId,
        collect,
        reference,
        content_uri,
        metadata: _metadata,
        onChainType,
      });
    } else {
      return await this.addActivityRest({
        communityId,
        content_uri,
        metadata: _metadata,
      });
    }
  }

  async addActivityRest({
    communityId,
    content_uri,
    metadata,
  }: {
    communityId: string;
    metadata: any;
    content_uri: string;
  }) {
    const res = await this.client.request.call(["activities", "addActivity"], {
      community_id: communityId,
      content_uri,
      content:
        typeof metadata !== "string" ? JSON.stringify(metadata) : metadata,
    });
    return res;
  }

  async createActivityPostWithSig({
    communityId,
    reference,
    content_uri,
    metadata,
    onChainType,
  }: {
    collect?: CollectModule;
    reference?: ReferenceModule;
    content_uri: string;
    communityId: string;
    metadata: any;
    onChainType?: OnChainTypeEnum;
  }) {
    const body = {
      community_id: communityId,
      content_uri,
      collect_module:
        {
          type: "FREE_COLLECT_MODULE",
          init: {
            free_collect_module: {
              only_follower: true,
            },
          },
        } || collect,
      reference_module:
        {
          type: "NULL_REFERENCE_MODULE",
          init: {
            null_reference_module: {},
          },
        } || reference,
    };
    const { data, error } = await this.client.typedata.activityCreate(body);
    if (error) return { error };

    const {
      type_data,
      type_data: { message },
    } = data;
    const { data: signature, error: errorSig } =
      await this.client.wallet.signMessage(type_data);
    if (errorSig) return { error: errorSig, data: null };
    const { r, s, v } = ethers.utils.splitSignature(signature);

    try {
      return new Promise((resolve, reject) => {
        let data1, data2;
        this.client.wallet.contract.methods
          .postWithSig({
            profileId: message.profileId,
            communityId: message.communityId,
            contentURI: message.contentURI,
            collectModule: message.collectModule,
            collectModuleInitData: message.collectModuleInitData,
            referenceModule: message.referenceModule,
            referenceModuleInitData: message.referenceModuleInitData,
            sig: {
              v,
              r,
              s,
              deadline: message.deadline,
            },
          })
          .send({}, async (data) => {
            if (data && data?.code !== 200) {
              return resolve({
                data: null,
                error: { ...data, code: WALLET_ERROR[data?.code] },
              });
            }

            if (onChainType === OnChainTypeEnum.ON_CHAIN) {
              const res = await this.addActivityRest({
                communityId,
                content_uri,
                metadata,
              });

              data1 = res?.data;
              if (data1 && data2) {
                data1.content_id = this.getContentId(data2);
                resolve({ data: data1 });
              }
            }
          })
          .then(async (data) => {
            await this.client.request.call(["tx", "txStore"], {
              tx_hash: data?.transactionHash,
            });
            if (onChainType === OnChainTypeEnum.ON_CHAIN) {
              data2 = data;
              if (data1 && data2) {
                data1.content_id = this.getContentId(data2);
                resolve({ data: data1 });
              }
            } else {
              resolve({ data: { content_id: this.getContentId(data) } });
            }
          })
          .catch((error) => {
            return {
              data: null,
              error: { ...error, code: WALLET_ERROR[error?.code] },
            };
          });
        if (data.error) {
          return data;
        }
      });
    } catch (error: string) {
      return { data: null, error };
    }
  }

  /**
   * Reads the feed
   * @link https://getstream.io/activity-feeds/docs/node/adding_activities/?language=js#retrieving-activities
   * @method get
   * @memberof StreamFeed.prototype
   * @param {GetFeedOptions} options  Additional options
   * @return {Promise<FeedAPIResponse>}
   * @example feed.get({limit: 10, id_lte: 'activity-id'})
   * @example feed.get({limit: 10, mark_seen: true})
   */
  async stories(profileId: string, query: {}) {
    const res = await this.client.request.call(
      ["profiles", "listDynamic"],
      profileId,
      query
    );
    return res;
  }

  async feeds(kind = "aggregated", type = "home", query = {}) {
    const enrichOption = {
      enrich: true,
      withRecentReactions: true,
      recentReactionsLimit: 20,
      withOwnReactions: true,
      withOwnChildren: true,
      withReactionCounts: true,
      reactionKindsFilter: ["COMMENT"],
      with_profile: false,
      with_community: false,
      with_relations: ["JOIN"],
    };
    const params = {
      a_community: query?.communityId,
      a_profile: query?.profileId,
    };
    const obj = {};
    const obj1 = {};
    Object.keys(enrichOption).forEach(
      (val) => (obj[`enrichOption.${val}`] = enrichOption[val])
    );
    Object.keys(params).forEach(
      (val) => (obj1[`params[${val}]`] = params[val])
    );

    const res = await this.client.request.call(
      ["feed", "listFeed"],
      FeedKindEnum[kind],
      FeedTypeEnum[type],
      {
        ...obj,
        ...obj1,

        limit: 10,
        reverse: true,
        next_token: "",
        exclude_view_ids: [""],
        ranking: "ACTIVITY_TIME",
        mark_read: true,
        ...query,
      }
    );
    return res;
  }

  async getFeedDetail(feedId: string, query = {}) {
    const res = await this.client.request.call(
      ["feed", "getFeed"],
      feedId,
      query
    );
    return res;
  }
}
