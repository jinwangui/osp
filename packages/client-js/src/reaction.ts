// @ts-nocheck

import { CollectType, ReferenceType } from "./activity";
import { DefaultGenerics, Client } from "./client";
import { WALLET_ERROR, APPID, OnChainTypeEnum } from "./constant";
import { getMainContentFocus } from "./util/getMainContentFocus";
import getUuid from "uuid-by-string";

import {
  ReactionKindEnum,
  AddReactionRequest,
  CollectModule,
  CollectModuleEnum,
  ContentTypeEnum,
  ReferenceModuleParam,
  ReferenceModuleEnum,
  ReferenceModule,
  CollectModuleParam,
  ListReactionRequest,
} from "./rest_api_generated";
import { ethers } from "ethers";

export type ReactionAddActivity = {
  target_user_id: string;
  target_activity_id: string;
  content_uri: string;
  reference_module_param: ReferenceModuleParam;
};
export type ReactionAddOptions = {
  userId: string;
  collect: CollectModule;
  reference: ReferenceModule;
  collectParam: any;
  referenceParam: any;
  isOnChain: boolean;
};

export type ReactionCreateActivity = {
  target_user_id: string;
  target_activity_id: string;
  content_uri: string;
  reference_module_param: ReferenceModuleParam;
};
export type ReactionCreateOptions = {
  user_id: string;
  collect_module?: CollectModule;
  reference_module?: ReferenceModule;
  collect_module_param: CollectModuleParam;
  reference_module_param: ReferenceModuleParam;
};
const mockCollectModule = {
  type: CollectModuleEnum.FeeCollectModule,
  init: {
    FreeCollectModule: {
      only_follower: true,
    },
  },
};
const mocReferenceModule = {
  type: ReferenceModuleEnum.FollowerOnlyReferenceModule,
  init: {},
};

const mockCollectModuleParam = {
  type: CollectModuleEnum.FeeCollectModule,
  data: {},
};

const mocReferenceModuleParam = {
  type: ReferenceModuleEnum.FollowerOnlyReferenceModule,
  data: {},
};

export class Reaction<
  StreamFeedGenerics extends DefaultGenerics = DefaultGenerics
> {
  client: Client<StreamFeedGenerics>;
  constructor(client: Client<StreamFeedGenerics>) {
    this.client = client;
  }

  async add(
    kind: ReactionKindEnum | ContentTypeEnum,
    {
      content_id: referenced_content_id,
      view_id: referenced_view_id,
      profile_id: referenced_profile_id,
      isOnChain,
      onChainType,
      metadata,
      reaction_kind,
    },
    params = {}
  ) {
    let content_uri = "";
    let _metadata;
    if (metadata) {
      _metadata = {
        ...{
          version: "2.0.0",
          metadata_id: getUuid(JSON.stringify(metadata)),
          external_url: `https://lenster.xyz/u/${this.client.profile.handle}`,
          image: null,
          imageMimeType: null,
          name: `Comment by ${this.client.profile.handle}`,
          tags: [],
          animation_url: null,
          mainContentFocus: getMainContentFocus(metadata),
          contentWarning: null,
          attributes: [
            {
              traitType: "type",
              displayType: "string",
              value: getMainContentFocus(metadata),
            },
          ],
          medias: [],
          locale: "zh-CN",
          appId: APPID,
        },
        ...metadata,
      };
      content_uri = await this.client.ipfs.upload(JSON.stringify(_metadata));
    }

    if (isOnChain) {
      if (content_uri) {
        return await this.addContentWithSig(
          kind,
          {
            referenced_content_id,
            referenced_view_id,
            referenced_profile_id,
            content_uri,
            reaction_kind,
            metadata: _metadata,
            onChainType,
          },
          params
        );
      } else {
        return await this.addWithSig(
          kind,
          { referenced_content_id, referenced_view_id, referenced_profile_id },
          params
        );
      }
    } else {
      return await this.addRest(kind, {
        referenced_view_id,
        referenced_profile_id,
        content_uri,
        metadata: _metadata,
      });
    }
  }
  private getContentId(data) {
    const content_id =
      data?.events["ContentReactionCreated"]?.returnValues?.contentId;
    return `0x${Number(content_id).toString(16)}`;
  }

  async addContentWithSig(
    kind: ContentTypeEnum,
    {
      referenced_content_id,
      referenced_view_id,
      referenced_profile_id,
      content_uri,
      metadata,
      reaction_kind,
      onChainType,
    }: ReactionAddActivity,
    params
  ) {
    const body = {
      referenced_profile_id,
      referenced_content_id,
      content_uri,
      kind,
      reference_module: {
        type: "NULL_REFERENCE_MODULE",
        init: {
          null_reference_module: {},
        },
      },
      reference_module_param: {
        type: "NULL_REFERENCE_MODULE",
        data: {
          null_reference_module: {},
        },
      },
    };

    const { data, error } = await this.client.typedata.contentReactionCreate(
      body
    );
    if (error) return { error };

    const {
      type_data,
      type_data: { message },
    } = data;
    const { data: signature, error: errorSig } =
      await this.client.wallet.signMessage(type_data);
    if (errorSig) return { error: errorSig, data: null };
    const { r, s, v } = ethers.utils.splitSignature(signature);

    try {
      return new Promise((resolve, reject) => {
        let data1, data2;
        this.client.wallet.contract.methods
          .addContentReactionWithSig({
            profileId: message.profileId,
            contentURI: message.contentURI,
            reactionType: message.reactionType,
            referencedProfileId: message.referencedProfileId,
            referencedContentId: message.referencedContentId,
            referenceModuleData: message.referenceModuleData,
            referenceModule: message.referenceModule,
            referenceModuleInitData: message.referenceModuleInitData,
            data: message.data,
            sig: {
              v,
              r,
              s,
              deadline: message.deadline,
            },
          })
          .send({}, async (data) => {
            if (data && data?.code !== 200) {
              return resolve({
                data: null,
                error: { ...data, code: WALLET_ERROR[data?.code] },
              });
            }
            if (onChainType === OnChainTypeEnum.ON_CHAIN) {
              const res = await this.addRest(reaction_kind, {
                referenced_view_id,
                referenced_profile_id,
                content_uri,
                metadata,
              });

              data1 = res?.data;
              if (data1 && data2) {
                data1.content_id = this.getContentId(data2);
                resolve({ data: data1 });
              }
            }
          })
          .then(async (data) => {
            await this.client.request.call(["tx", "txStore"], {
              tx_hash: data?.transactionHash,
            });

            if (onChainType === OnChainTypeEnum.ON_CHAIN) {
              data2 = data;
              if (data1 && data2) {
                data1.content_id = this.getContentId(data2);
                resolve({ data: data1 });
              }
            } else {
              resolve({ data: { content_id: this.getContentId(data) } });
            }
          })
          .catch((error) => {
            resolve({
              data: null,
              error: { ...error, code: WALLET_ERROR[error?.code] },
            });
          });
      });
    } catch (error: string) {
      return { data: null, error };
    }

    return { error: "" };
  }

  async addWithSig(
    kind: ReactionKindEnum,
    {
      referenced_content_id,
      referenced_view_id,
      referenced_profile_id,
      onChainType,
    }: ReactionAddActivity,
    params
  ) {
    const body = {
      referenced_profile_id,
      referenced_content_id,
      kind,
      reference_module_param: {
        type: "NULL_REFERENCE_MODULE",
        data: {
          null_reference_module: {},
        },
      },
      reaction_param: params,
    };
    const { data, error } = await this.client.typedata.reactionCreate(body);
    if (error) return { error };

    const {
      type_data,
      type_data: { message },
    } = data;
    const { data: signature, error: errorSig } =
      await this.client.wallet.signMessage(type_data);
    if (errorSig) return { error: errorSig, data: null };
    const { r, s, v } = ethers.utils.splitSignature(signature);

    try {
      return new Promise((resolve, reject) => {
        this.client.wallet.contract.methods
          .addReactionWithSig({
            profileId: message.profileId,
            reactionType: message.reactionType,
            referencedProfileId: message.referencedProfileId,
            referencedContentId: message.referencedContentId,
            referenceModuleData: message.referenceModuleData,
            data: message.data,
            sig: {
              v,
              r,
              s,
              deadline: message.deadline,
            },
          })
          .send({}, async (data) => {
            if (data && data?.code !== 200) {
              return resolve({
                data: null,
                error: { ...data, code: WALLET_ERROR[data?.code] },
              });
            }
            if (onChainType === OnChainTypeEnum.ON_CHAIN) {
              const res = await this.addRest(kind, {
                referenced_view_id,
                referenced_profile_id,
              });
              resolve(res);
            } else {
              resolve({ data: true, error: null });
            }
          })
          .then((data) => {})
          .catch((error) => {
            resolve({
              data: null,
              error: { ...error, code: WALLET_ERROR[error?.code] },
            });
          });
      });
    } catch (error: string) {
      return { data: null, error };
    }
  }

  /**
   * add reaction
   * @link https://getstream.io/activity-feeds/docs/node/reactions_introduction/?language=js#adding-reactions
   * @method add
   * @memberof StreamReaction.prototype
   * @param  {string}   kind  kind of reaction
   * @param  {string}   activity Activity or an ActivityID
   * @param  {ReactionType}   data  data related to reaction
   * @param  {ReactionAddOptions} [options]
   * @param  {string} [options.id] id associated with reaction
   * @param  {string[]} [options.targetFeeds] an array of feeds to which to send an activity with the reaction
   * @param  {string} [options.userId] useful for adding reaction with server token
   * @param  {object} [options.targetFeedsExtraData] extra data related to target feeds
   * @return {Promise<ReactionAPIResponse<ReactionType>>}
   * @example reactions.add("like", "0c7db91c-67f9-11e8-bcd9-fe00a9219401")
   * @example reactions.add("comment", "0c7db91c-67f9-11e8-bcd9-fe00a9219401", {"text": "love it!"},)
   */
  async addRest(
    kind: ReactionKindEnum,
    {
      referenced_view_id,
      content_uri,
      referenced_profile_id,
      metadata,
    }: ReactionAddActivity
  ) {
    const body: AddReactionRequest = {
      kind,
      referenced_view_id,
      content_uri,
      referenced_profile_id,
      content:
        metadata && typeof metadata !== "string"
          ? JSON.stringify(metadata)
          : metadata,
    };

    const res = await this.client.request.call(
      ["reactions", "addReaction"],
      body
    );
    return res;
  }

  /**
   * get all reaction
   * @link https://getstream.io/activity-feeds/docs/node/reactions_introduction/?language=js#retrieving-reactions
   * @method get
   * @memberof StreamReaction.prototype
   * @param  {string}   id Reaction Id
   * @return {Promise<EnrichedReactionAPIResponse<StreamFeedGenerics>>}
   * @example reactions.get("ACTIVITY","67b3e3b5-b201-4697-96ac-482eb14f88ec","COMMENT", {limit:20})
   */
  async get(
    lookupAttr: string,
    lookupValue: string,
    kind: ReactionKindEnum,
    data: { ranking: any; limit: any; withActivity: any }
  ) {
    const query = {
      kind: kind,
      ranking: data?.ranking || "TIME",
      limit: data?.limit || 10,
      with_activity: data?.withActivity || false,
      with_child_count: true,
      ...data,
    } as any;

    const res = await this.client.request.call(
      ["reactions", "getReactions"],
      lookupAttr,
      lookupValue,
      kind,
      query
    );
    return res;
  }
  /**
   * get reaction detail
   * @link https://getstream.io/activity-feeds/docs/node/reactions_introduction/?language=js#retrieving-reactions
   * @method get
   * @memberof StreamReaction.prototype
   * @param  {string}   id Reaction Id
   * @return {Promise<EnrichedReactionAPIResponse<StreamFeedGenerics>>}
   * @example reactions.get("67b3e3b5-b201-4697-96ac-482eb14f88ec")
   */
  async getDetail(id: string) {
    const res = await this.client.request.call.reactions.getReaction(id);
    return res;
  }
  /**
   * delete reaction
   * @link https://getstream.io/activity-feeds/docs/node/reactions_introduction/?language=js#removing-reactions
   * @method delete
   * @memberof StreamReaction.prototype
   * @param  {string}   id Reaction Id
   * @return {Promise<APIResponse>}
   * @example reactions.delete("ACTIVITY","67b3e3b5-b201-4697-96ac-482eb14f88ec","COMMENT")
   */
  async deleteByKind(
    lookupAttr: string,
    lookupValue: string,
    kind: ReactionKindEnum
  ) {
    const res = await this.client.request.call(
      ["reactions", "deleteReactionByKind"],
      lookupAttr,
      lookupValue,
      kind
    );
    return res;
  }
}
