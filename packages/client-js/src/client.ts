import { Activity } from "./activity";
import { Relation } from "./relation";
import { Profile } from "./profile";
import { Community } from "./community";
import { Reaction } from "./reaction";
import { TypeData } from "./typedata";
import { Auth } from "./auth";
import { IPFS } from "./ipfs";
import { Wallet } from "./wallet";
import { Request } from "./request";
import { NFT } from "./nft";
import { AbiItem } from "web3-utils";

// require('./lib/metamask-sdk.js')

export type UR = Record<string, unknown>;
export type DefaultGenerics = {
  activityType: UR;
  childReactionType: UR;
  collectionType: UR;
  personalizationType: UR;
  reactionType: UR;
  userType: UR;
};

export type ClientOptions = {
  urlOverride?: Record<string, string>;
  timeout?: number;
  browser?: boolean;
  version?: string;
  cache?: boolean;
  cacheProvider?: any;
  error?: () => void;
  imageurl?: string;
};

export type ContractConfig = {
  abi?: AbiItem[];
  gasPrice?: string;
};

export class Client<
  StreamFeedGenerics extends DefaultGenerics = DefaultGenerics
> {
  baseUrl: string;
  apiKey: string;
  appId?: string;
  apiSecret: string | null;
  userId?: string;
  imageurl: string;
  version: string;
  options: ClientOptions;
  enableCache: boolean;
  isBrowser: boolean;
  // 不确定
  cache: any;
  wallet: any;
  profile: any;
  community: any;
  nft: any;
  address: string;
  request: Request;
  errorHandler?: () => void;
  ipfs: IPFS;
  activity: Activity<StreamFeedGenerics>;
  relation: Relation<StreamFeedGenerics>;
  reaction: Reaction<StreamFeedGenerics>;
  auth: Auth<StreamFeedGenerics>;
  typedata: TypeData<StreamFeedGenerics>;
  // reactions: StreamReaction<StreamFeedGenerics>;
  constructor(
    apiKey: string,
    apiSecretOrToken: string | null,
    appId?: string,
    options: ClientOptions = {}
  ) {
    this.apiKey = apiKey;
    this.apiSecret = apiSecretOrToken;

    this.appId = appId;
    this.options = options;
    this.version = this.options.version || "v1.0";
    this.enableCache = this.options.cache || true;
    this.baseUrl = this.getBaseUrl();
    this.imageurl = this.options.imageurl || "https://test.trexprotocol.com";
    this.isBrowser =
      typeof location !== "undefined" && this.options.browser ? true : false;
    this.cache =
      typeof localStorage !== "undefined"
        ? localStorage
        : this.options.cacheProvider;
    this.wallet =
      typeof location !== "undefined" && options.browser
        ? new Wallet(this)
        : {};

    this.request = new Request(this);
    if (options.error) {
      this.errorHandler = options.error;
    }

    this.ipfs = new IPFS(this);
    this.nft = new NFT(this);
    this.activity = new Activity<StreamFeedGenerics>(this);
    this.relation = new Relation<StreamFeedGenerics>(this);
    this.profile = new Profile<StreamFeedGenerics>(this);
    this.community = new Community<StreamFeedGenerics>(this);
    this.reaction = new Reaction<StreamFeedGenerics>(this);
    this.auth = new Auth<StreamFeedGenerics>(this);
    this.typedata = new TypeData<StreamFeedGenerics>(this);
  }
  getBaseUrl(serviceName?: string) {
    if (!serviceName) serviceName = "api";

    if (this.options.urlOverride && this.options.urlOverride[serviceName])
      return this.options.urlOverride[serviceName];

    return this.baseUrl;
  }

  // initAbi(abi: AbiItem[],gasPrice='20000000000',from: string){
  //   this.contract = new this.web3.eth.Contract(abi, CONTRACH_ADDRESS, {
  //     from, // 默认交易发送地址
  //     gasPrice
  //   });
  // }
}
